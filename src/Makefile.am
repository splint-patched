## Note: starting comments with ## means they don't end up in Makefile

AUTOMAKE_OPTIONS = 1.5 foreign

bin_PROGRAMS = splint

INCDIR = Headers

AM_CPPFLAGS = -I$(srcdir)/$(INCDIR) -DPKGDATADIR='"$(pkgdatadir)"'
AM_YFLAGS = -d

PARSERS = \
	  cgrammar.y \
	  mtgrammar.y \
	  llgrammar.y \
	  lslgrammar.y

HAND_SCANNERS = \
		lclscan.c \
		mtscanner.c \
		lslscan.c

SCANNERS = \
	   cscanner.l \
	   $(HAND_SCANNERS)

GRAMSRC = $(PARSERS) $(SCANNERS)

## The main sources

SETSRC = \
	 globSet.c \
	 guardSet.c \
	 intSet.c \
	 sRefSet.c \
	 typeIdSet.c \
	 usymIdSet.c

LISTSRC = \
	  clauseStack.c \
          cstringList.c \
	  cstringSList.c \
	  ctypeList.c \
          enumNameList.c \
	  enumNameSList.c \
	  exprNodeList.c \
	  exprNodeSList.c \
	  filelocList.c \
	  filelocStack.c \
          flagMarkerList.c \
	  flagSpec.c \
	  idDeclList.c \
	  qualList.c \
	  sRefSetList.c \
          uentryList.c

CPPSRC = \
	 cppmain.c \
	 cpplib.c \
	 cppexp.c \
	 cpphash.c \
	 cpperror.c

CSRC = \
       clabstract.c \
       clause.c \
       context.c \
       cprim.c \
       cscannerHelp.c \
       ctype.c \
       functionClause.c \
       functionClauseList.c \
       functionConstraint.c \
       globalsClause.c \
       idDecl.c \
       macrocache.c \
       metaStateConstraint.c \
       metaStateConstraintList.c \
       metaStateExpression.c \
       metaStateSpecifier.c \
       modifiesClause.c \
       pointers.c \
       qtype.c \
       qual.c \
       stateClause.c \
       stateClauseList.c \
       uentry.c \
       warnClause.c

SPLINTSRC = \
	    exprChecks.c \
	    exprNode.c \
	    help.c \
	    llmain.c \
	    rcfiles.c

CHECKSRC = \
	   nameChecks.c \
	   reservedNames.c \
	   structNames.c \
	   transferChecks.c \
	   varKinds.c

GLOBSRC = \
	  globals.c \
	  flags.c \
	  general.c \
	  mstring.c \
	  misc.c \
	  osd.c \
	  reader.c \
	  mtreader.c

OVERFLOWCHSRC = \
		constraintList.c \
		constraintResolve.c \
                constraintGeneration.c \
		constraintTerm.c \
                constraintExprData.c \
		constraintExpr.c \
		constraint.c \
                loopHeuristics.c 

GENERALSRC = \
	     exprData.c \
	     cstring.c \
	     fileloc.c \
	     message.c \
	     inputStream.c \
             fileTable.c \
	     cstringHash.c \
	     cstringTable.c \
	     valueTable.c \
	     stateValue.c \
             llerror.c \
	     messageLog.c \
	     flagMarker.c \
	     aliasTable.c \
	     ynm.c \
             sRefTable.c \
	     genericTable.c \
	     ekind.c \
	     usymtab.c \
	     multiVal.c \
             lltok.c \
	     sRef.c \
	     library.c \
	     fileLib.c

METASTATESRC = \
	       stateInfo.c \
	       stateCombinationTable.c \
	       metaStateTable.c \
               metaStateInfo.c \
	       annotationTable.c \
	       annotationInfo.c \
	       mttok.c \
               mtDeclarationNode.c \
	       mtDeclarationPieces.c \
	       mtDeclarationPiece.c \
               mtContextNode.c \
	       mtValuesNode.c \
	       mtDefaultsNode.c \
               mtAnnotationsNode.c \
	       mtMergeNode.c \
	       mtAnnotationList.c \
               mtAnnotationDecl.c \
	       mtTransferClauseList.c \
	       mtTransferClause.c \
               mtTransferAction.c \
	       mtLoseReferenceList.c \
	       mtLoseReference.c \
               mtDefaultsDeclList.c \
	       mtDefaultsDecl.c \
	       mtMergeItem.c \
               mtMergeClause.c \
	       mtMergeClauseList.c

## These are only used if LCL is enabled

LCLONLYSRC = \
	     usymtab_interface.c \
	     abstract.c \
	     ltoken.c \
	     lclscanline.c \
             lclsyntable.c \
	     lcltokentable.c \
	     sort.c \
	     symtable.c \
	     lclinit.c \
             shift.c \
	     lsymbol.c \
	     mapping.c

LSLSRC = \
	 lsltokentable.c \
	 lslscanline.c \
	 lslparse.c \
         lh.c \
	 checking.c \
	 lclctypes.c \
	 imports.c \
	 lslinit.c \
	 lslsyntable.c 

LCLSETSRC  = \
	     lsymbolSet.c \
	     sigNodeSet.c \
	     lslOpSet.c \
	     sortSet.c

LCLLISTSRC = \
	     initDeclNodeList.c \
	     sortList.c \
	     declaratorInvNodeList.c \
             interfaceNodeList.c \
	     sortSetList.c \
	     declaratorNodeList.c \
             letDeclNodeList.c \
	     stDeclNodeList.c \
	     storeRefNodeList.c \
             lslOpList.c \
	     lsymbolList.c \
	     termNodeList.c \
	     ltokenList.c \
             traitRefNodeList.c \
	     pairNodeList.c \
	     typeNameNodeList.c \
             fcnNodeList.c \
	     paramNodeList.c \
	     programNodeList.c \
             varDeclarationNodeList.c \
	     varNodeList.c \
	     quantifierNodeList.c \
             replaceNodeList.c \
	     importNodeList.c

LCLSRC = \
	 $(LCLSETSRC) \
	 $(LCLLISTSRC) \
	 $(LSLSRC) \
	 $(LCLONLYSRC) 

COMMONSRC =  \
	     $(CPPSRC) \
	     $(CSRC) \
	     $(CHECKSRC) \
	     $(GENERALSRC) \
	     $(GLOBSRC) \
	     $(LISTSRC) \
	     $(SETSRC) \
	     $(METASTATESRC) \
	     $(OVERFLOWCHSRC) \
	     $(SPLINTSRC) \
	     $(LCLSRC)

HEADERSRC = \
	    $(INCDIR)/abstBodyNode.h \
	    $(INCDIR)/abstract.h \
	    $(INCDIR)/abstractNode.h \
	    $(INCDIR)/aliasTable.h \
	    $(INCDIR)/annotationInfo.h \
	    $(INCDIR)/annotationTable.h \
	    $(INCDIR)/arrayQualNode.h \
	    $(INCDIR)/basic.h \
	    $(INCDIR)/bool.h \
	    $(INCDIR)/checking.h \
	    $(INCDIR)/clabstract.h \
	    $(INCDIR)/claimNode.h \
	    $(INCDIR)/clause.h \
	    $(INCDIR)/clauseStack.h \
	    $(INCDIR)/code.h \
	    $(INCDIR)/constants.h \
	    $(INCDIR)/constDeclarationNode.h \
	    $(INCDIR)/constraintExprData.h \
	    $(INCDIR)/constraintExpr.h \
	    $(INCDIR)/constraint.h \
	    $(INCDIR)/constraintList.h \
	    $(INCDIR)/constraintOutput.h \
	    $(INCDIR)/constraintResolve.h \
	    $(INCDIR)/constraintTerm.h \
	    $(INCDIR)/context.h \
	    $(INCDIR)/cppconf.h \
	    $(INCDIR)/cpperror.h \
	    $(INCDIR)/cppexp.h \
	    $(INCDIR)/cpp.h \
	    $(INCDIR)/cpphash.h \
	    $(INCDIR)/cpplib.h \
	    $(INCDIR)/cprim.h \
	    $(INCDIR)/cscanner.h \
	    $(INCDIR)/cscannerHelp.h \
	    $(INCDIR)/cstring.h \
	    $(INCDIR)/cstringHash.h \
	    $(INCDIR)/cstringList.h \
	    $(INCDIR)/cstringSList.h \
	    $(INCDIR)/cstringTable.h \
	    $(INCDIR)/ctypeList.h \
	    $(INCDIR)/CTypesNode.h \
	    $(INCDIR)/declaratorInvNode.h \
	    $(INCDIR)/declaratorInvNodeList.h \
	    $(INCDIR)/declaratorNode.h \
	    $(INCDIR)/declaratorNodeList.h \
	    $(INCDIR)/ekind.h \
	    $(INCDIR)/enumNameList.h \
	    $(INCDIR)/enumNameSList.h \
	    $(INCDIR)/enumSpecNode.h \
	    $(INCDIR)/exportNode.h \
	    $(INCDIR)/exposedNode.h \
	    $(INCDIR)/exprChecks.h \
	    $(INCDIR)/exprData.h \
	    $(INCDIR)/exprNode.h \
	    $(INCDIR)/exprNodeList.h \
	    $(INCDIR)/exprNodeSList.h \
	    $(INCDIR)/fcnNode.h \
	    $(INCDIR)/fcnNodeList.h \
	    $(INCDIR)/fileId.h \
	    $(INCDIR)/fileIdList.h \
	    $(INCDIR)/fileLib.h \
	    $(INCDIR)/fileloc.h \
	    $(INCDIR)/filelocList.h \
	    $(INCDIR)/filelocStack.h \
	    $(INCDIR)/fileTable.h \
	    $(INCDIR)/flagMarker.h \
	    $(INCDIR)/flagMarkerList.h \
	    $(INCDIR)/flags.h \
	    $(INCDIR)/flagSpec.h \
	    $(INCDIR)/forwardTypes.h \
	    $(INCDIR)/functionClause.h \
	    $(INCDIR)/functionClauseList.h \
	    $(INCDIR)/functionConstraint.h \
	    $(INCDIR)/general.h \
	    $(INCDIR)/genericTable.h \
	    $(INCDIR)/globalList.h \
	    $(INCDIR)/globalsClause.h \
	    $(INCDIR)/globals.h \
	    $(INCDIR)/globSet.h \
	    $(INCDIR)/guardSet.h \
	    $(INCDIR)/help.h \
	    $(INCDIR)/idDecl.h \
	    $(INCDIR)/idDeclList.h \
	    $(INCDIR)/importNode.h \
	    $(INCDIR)/importNodeList.h \
	    $(INCDIR)/imports.h \
	    $(INCDIR)/initDeclNode.h \
	    $(INCDIR)/initDeclNodeList.h \
	    $(INCDIR)/inputStream.h \
	    $(INCDIR)/interfaceNode.h \
	    $(INCDIR)/interfaceNodeList.h \
	    $(INCDIR)/intSet.h \
	    $(INCDIR)/iterNode.h \
	    $(INCDIR)/lcl_constants.h \
	    $(INCDIR)/lclctypes.h \
	    $(INCDIR)/lclForwardTypes.h \
	    $(INCDIR)/lclinit.h \
	    $(INCDIR)/library.h \
	    $(INCDIR)/lclMisc.h \
	    $(INCDIR)/lclPredicateNode.h \
	    $(INCDIR)/lclscan.h \
	    $(INCDIR)/lclscanline.h \
	    $(INCDIR)/lclsyntable.h \
	    $(INCDIR)/lcltokentable.h \
	    $(INCDIR)/lclTypeSpecNode.h \
	    $(INCDIR)/lctype.h \
	    $(INCDIR)/letDeclNode.h \
	    $(INCDIR)/letDeclNodeList.h \
	    $(INCDIR)/lh.h \
	    $(INCDIR)/llerror.h \
	    $(INCDIR)/llglobals.h \
	    $(INCDIR)/llmain.h \
	    $(INCDIR)/lltok.h \
	    $(INCDIR)/lslinit.h \
	    $(INCDIR)/lslOp.h \
	    $(INCDIR)/lslOpList.h \
	    $(INCDIR)/lslOpSet.h \
	    $(INCDIR)/lslparse.h \
	    $(INCDIR)/lslscan.h \
	    $(INCDIR)/lslscanline.h \
	    $(INCDIR)/lslsyntable.h \
	    $(INCDIR)/lsltokentable.h \
	    $(INCDIR)/lsymbol.h \
	    $(INCDIR)/lsymbolList.h \
	    $(INCDIR)/lsymbolSet.h \
	    $(INCDIR)/ltoken.h \
	    $(INCDIR)/ltokenList.h \
	    $(INCDIR)/macrocache.h \
	    $(INCDIR)/mapping.h \
	    $(INCDIR)/message.h \
	    $(INCDIR)/messageLog.h \
	    $(INCDIR)/metaStateConstraint.h \
	    $(INCDIR)/metaStateConstraintList.h \
	    $(INCDIR)/metaStateExpression.h \
	    $(INCDIR)/metaStateInfo.h \
	    $(INCDIR)/metaStateSpecifier.h \
	    $(INCDIR)/metaStateTable.h \
	    $(INCDIR)/misc.h \
	    $(INCDIR)/modifiesClause.h \
	    $(INCDIR)/modifyNode.h \
	    $(INCDIR)/mstring.h \
	    $(INCDIR)/mtAnnotationDecl.h \
	    $(INCDIR)/mtAnnotationList.h \
	    $(INCDIR)/mtAnnotationsNode.h \
	    $(INCDIR)/mtContextNode.h \
	    $(INCDIR)/mtDeclarationNode.h \
	    $(INCDIR)/mtDeclarationPiece.h \
	    $(INCDIR)/mtDeclarationPieces.h \
	    $(INCDIR)/mtDefaultsDecl.h \
	    $(INCDIR)/mtDefaultsDeclList.h \
	    $(INCDIR)/mtDefaultsNode.h \
	    $(INCDIR)/mtincludes.h \
	    $(INCDIR)/mtLoseReference.h \
	    $(INCDIR)/mtLoseReferenceList.h \
	    $(INCDIR)/mtMergeClause.h \
	    $(INCDIR)/mtMergeClauseList.h \
	    $(INCDIR)/mtMergeItem.h \
	    $(INCDIR)/mtMergeNode.h \
	    $(INCDIR)/mtreader.h \
	    $(INCDIR)/mtscanner.h \
	    $(INCDIR)/mttok.h \
	    $(INCDIR)/mtTransferAction.h \
	    $(INCDIR)/mtTransferClause.h \
	    $(INCDIR)/mtTransferClauseList.h \
	    $(INCDIR)/mtValuesNode.h \
	    $(INCDIR)/multiVal.h \
	    $(INCDIR)/nameChecks.h \
	    $(INCDIR)/nameNode.h \
	    $(INCDIR)/opFormNode.h \
	    $(INCDIR)/osd.h \
	    $(INCDIR)/pairNode.h \
	    $(INCDIR)/pairNodeList.h \
	    $(INCDIR)/paramNode.h \
	    $(INCDIR)/paramNodeList.h \
	    $(INCDIR)/pointers.h \
	    $(INCDIR)/privateNode.h \
	    $(INCDIR)/programNode.h \
	    $(INCDIR)/programNodeList.h \
	    $(INCDIR)/qtype.h \
	    $(INCDIR)/qual.h \
	    $(INCDIR)/qualList.h \
	    $(INCDIR)/quantifiedTermNode.h \
	    $(INCDIR)/quantifierNode.h \
	    $(INCDIR)/quantifierNodeList.h \
	    $(INCDIR)/rcfiles.h \
	    $(INCDIR)/reader.h \
	    $(INCDIR)/renamingNode.h \
	    $(INCDIR)/replaceNode.h \
	    $(INCDIR)/replaceNodeList.h \
	    $(INCDIR)/reservedNames.h \
	    $(INCDIR)/shift.h \
	    $(INCDIR)/signNode.h \
	    $(INCDIR)/sigNode.h \
	    $(INCDIR)/sigNodeSet.h \
	    $(INCDIR)/sort.h \
	    $(INCDIR)/sortList.h \
	    $(INCDIR)/sortSet.h \
	    $(INCDIR)/sortSetList.h \
	    $(INCDIR)/sRef.h \
	    $(INCDIR)/sRefSet.h \
	    $(INCDIR)/sRefSetList.h \
	    $(INCDIR)/sRefTable.h \
	    $(INCDIR)/stateClause.h \
	    $(INCDIR)/stateClauseList.h \
	    $(INCDIR)/stateCombinationTable.h \
	    $(INCDIR)/stateInfo.h \
	    $(INCDIR)/stateValue.h \
	    $(INCDIR)/stDeclNode.h \
	    $(INCDIR)/stDeclNodeList.h \
	    $(INCDIR)/stmtNode.h \
	    $(INCDIR)/storeRefNode.h \
	    $(INCDIR)/storeRefNodeList.h \
	    $(INCDIR)/strOrUnionNode.h \
	    $(INCDIR)/structNames.h \
	    $(INCDIR)/symtable.h \
	    $(INCDIR)/system_constants.h \
	    $(INCDIR)/taggedUnionNode.h \
	    $(INCDIR)/termNode.h \
	    $(INCDIR)/termNodeList.h \
	    $(INCDIR)/traitRefNode.h \
	    $(INCDIR)/traitRefNodeList.h \
	    $(INCDIR)/transferChecks.h \
	    $(INCDIR)/typeExpr.h \
	    $(INCDIR)/typeId.h \
	    $(INCDIR)/typeIdSet.h \
	    $(INCDIR)/typeNameNode.h \
	    $(INCDIR)/typeNameNodeList.h \
	    $(INCDIR)/typeNamePack.h \
	    $(INCDIR)/typeNode.h \
	    $(INCDIR)/uentry.h \
	    $(INCDIR)/uentryList.h \
	    $(INCDIR)/usymId.h \
	    $(INCDIR)/usymIdSet.h \
	    $(INCDIR)/usymtab.h \
	    $(INCDIR)/usymtab_interface.h \
	    $(INCDIR)/valueTable.h \
	    $(INCDIR)/varDeclarationNode.h \
	    $(INCDIR)/varDeclarationNodeList.h \
	    $(INCDIR)/varKinds.h \
	    $(INCDIR)/varNode.h \
	    $(INCDIR)/varNodeList.h \
	    $(INCDIR)/libversion.h \
	    $(INCDIR)/warnClause.h \
	    $(INCDIR)/ynm.h

splint_SOURCES =  $(COMMONSRC) $(GRAMSRC) $(HEADERSRC)

dist_bin_SCRIPTS = genflagcodes

## Sources that need to be built.
## Automake knows how to handle yacc generated C files, but for C headers
## it needs to have these added explicitly for correct dependency tracking.
BUILT_SOURCES = \
		$(srcdir)/cgrammar.h \
		$(srcdir)/mtgrammar.h \
		$(srcdir)/llgrammar.h \
		$(srcdir)/lslgrammar.h \
		$(srcdir)/flag_codes.h

IFILES = \
	 ctbase.i \
	 cttable.i

EXTRAGRAMMARFILES = bison.head bison.reset flex.head flex.reset

EXTRA_DIST = \
	     flags.def \
	     $(srcdir)/flag_codes.h \
	     $(INCDIR)/splintMacros.nf \
	     $(EXTRAGRAMMARFILES) \
	     $(IFILES) \
	     .splintrc \
	     LICENSE


## Hand-generated 
$(srcdir)/flag_codes.h: flags.def genflagcodes
	@if $(AM_V_P); then set -x; else echo "  GEN      " $(notdir $@); fi; \
		$(srcdir)/genflagcodes "$<" "$@.temp"; \
		if test $$? -eq 0; then \
			if test -f "$@" && cmp -s "$@" "$@.temp"; then \
				echo $(notdir $@) "is unchanged"; \
				rm -f "$@.temp"; \
			else \
				echo "updating" $(notdir $@); \
				mv -f "$@.temp" "$@"; \
			fi; \
		else \
			exit 1; \
		fi;


##############
# Self-check #
##############

# files to run Splint on; some scanners are hand-written and shouldn't
# cause (much) problems; lex/yacc generated files don't play nice and
# thus need some pampering (with some prefixing/suffixing).
#
# TODO: these files have been disactivated for some time; I (mg) might
# have removed some of the inclusion of these head/reset files from the
# source *.{l,y} files.
CHK_GRAMSRC = $(HAND_SCANNERS)

## original used LFLAGS=-L to suppress #line from output
# LFLAGS = -L
# chk_cscanner.c: cscanner.c
#	$(CAT) flex.head $< flex.reset > $@
#
# CHK_GRAMSRC += chk_cscanner.c
# EXTRA_DIST += flex.head flex.reset
#
# chk_lslgrammar.c: lslgrammar.c
#	$(CAT) bison.head $< bison.reset > $@
# chk_lslgrammar.h: lslgrammar.h
#	$(CAT) bison.head $< bison.reset > $@
# CHK_GRAMSRC += chk_lslgrammar.c
#
# chk_llgrammar.c: llgrammar.c
#	$(CAT) bison.head $< bison.reset > $@
# chk_llgrammar.h: llgrammar.h
#	$(CAT) bison.head $< bison.reset > $@
# CHK_GRAMSRC += chk_llgrammar.c
#
# chk_mtgrammar.c: mtgrammar.c
#	$(CAT) bison.head $< bison.reset > $@
# chk_mtgrammar.h: mtgrammar.h
#	$(CAT) bison.head $< bison.reset > $@
# CHK_GRAMSRC += chk_mtgrammar.c
#
# chk_cgrammar.c: cgrammar.c
#	$(CAT) bison.head $< bison.reset > $@
# chk_cgrammar.h: cgrammar.h
#	$(CAT) bison.head $< bison.reset > $@
# CHK_GRAMSRC += chk_cgrammar.c
#
# EXTRA_DIST += bison.head bison.reset

LINTSRC = $(COMMONSRC) $(CHK_GRAMSRC)

# Define INTERCEPT environment variable before calling make to interpose some
# other program before splint, e.g. 
# INTERCEPT = valgrind -v --leak-resolution=high --num-callers=20 \
#   	--show-reachable=no --leak-check=yes

RUNSPLINT = $(INTERCEPT) $(builddir)/splint \
	 -mts file -mts filerw -mts tainted \
	 $(DEFAULT_INCLUDES) $(DEFS) $(INCLUDES) $(AM_CPPFLAGS) $(CPPFLAGS)

.PHONY: splintme
splintme: 
	$(RUNSPLINT) -dump splint $(LINTSRC)

.PHONY: splintme-bounds
splintme-bounds: 
	$(RUNSPLINT) -dump splint +bounds +impboundsconstraints $(LINTSRC)

.PHONY: splintme-supcounts
splintme-supcounts: 
	$(RUNSPLINT) -dump splint -unrecogcomments -supcounts $(LINTSRC)

.PHONY: splintme-bufchk
splintme-bufchk: 
	$(RUNSPLINT) -dump splint -unrecogcomments -supcounts +bounds -DLINTBUFFERCHECK $(LINTSRC) 

# Example of a partial (one file) analysis
.PHONY: splintme-some
splintme-some: 
	$(RUNSPLINT) -load splint -unrecogcomments -supcounts +partial library.c


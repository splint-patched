/*
** Splint - annotation-assisted static program checker
** Copyright (C) 1994-2003 University of Virginia,
**         Massachusetts Institute of Technology
**
** This program is free software; you can redistribute it and/or modify it
** under the terms of the GNU General Public License as published by the
** Free Software Foundation; either version 2 of the License, or (at your
** option) any later version.
** 
** This program is distributed in the hope that it will be useful, but
** WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** General Public License for more details.
** 
** The GNU General Public License is available from http://www.gnu.org/ or
** the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
** MA 02111-1307, USA.
**
** For information on splint: info@splint.org
** To report a bug: splint-bug@splint.org
** For more information: http://www.splint.org
*/

# include "splintMacros.nf"
# include "basic.h"

bool mstring_containsChar (const char *s, char c)
{
  if (mstring_isDefined (s))
    {
      return (strchr (s, c) != NULL);
    }
  else
    {
      return FALSE;
    }
}

bool mstring_containsString (const char *s, const char *c)
{
  if (mstring_isDefined (s))
    {
      return (strstr (s, c) != NULL);
    }
  else
    {
      return FALSE;
    }
}
 
char *mstring_concat (const char *s1, const char *s2)
{
  char *s = (char *) dmalloc (strlen (s1) + strlen (s2) + 1);
  strcpy (s, s1);
  strcat (s, s2);
  return s;
}

/*@only@*/ char *
mstring_concatFree (/*@only@*/ char *s1, /*@only@*/ char *s2)
{
  /* like mstring_concat but deallocates old strings */
  char *s = (char *) dmalloc (strlen (s1) + strlen (s2) + 1);
  strcpy (s, s1);
  strcat (s, s2);

  sfree (s1);
  sfree (s2);
  return s;
}

/*@only@*/ char *
mstring_concatFree1 (/*@only@*/ char *s1, const char *s2)
{
  char *s = (char *) dmalloc (strlen (s1) + strlen (s2) + 1);
  strcpy (s, s1);
  strcat (s, s2);
  sfree (s1);

  return s;
}

/*@only@*/ char *
mstring_append (/*@only@*/ char *s1, char c)
{
  size_t l = strlen (s1);
  char *s;

  s = (char *) dmalloc (sizeof (*s) * (l + 2));

  strcpy (s, s1);
  *(s + l) = c;
  *(s + l + 1) = '\0';
  sfree (s1); 
  return s;
}

char *
mstring_copy (const char *s1)
{
  if (s1 == NULL)
    {
      return NULL;
    }
  else
    {
      char *s = (char *) dmalloc ((strlen (s1) + 1) * sizeof (*s));
      strcpy (s, s1);
      return s;
    }
}

char *
mstring_create (size_t n)
{
  char *s;

  s = dmalloc (sizeof (*s) * (n + 1));
  *s = '\0';
  return s;
}

bool mstring_equalPrefix (const char *c1, const char *c2)
{
  llassert (c1 != NULL);
  llassert (c2 != NULL);

  if (strncmp(c1, c2, strlen(c2)) == 0)
    {
      return TRUE;
    }
  else
    {
      return FALSE;
    }
}

bool mstring_equal (/*@null@*/ const char *s1, /*@null@*/ const char *s2)
{
  if (s1 == NULL)
    {
      return (s2 == NULL);
    }
  else
    {
      if (s2 == NULL)
	{
	  return FALSE;
	}
      else
	{
	  return (strcmp(s1, s2) == 0);
	}
    }
}

/*
** Splint - annotation-assisted static program checker
** Copyright (C) 1994-2003 University of Virginia,
**         Massachusetts Institute of Technology
**
** This program is free software; you can redistribute it and/or modify it
** under the terms of the GNU General Public License as published by the
** Free Software Foundation; either version 2 of the License, or (at your
** option) any later version.
** 
** This program is distributed in the hope that it will be useful, but
** WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** General Public License for more details.
** 
** The GNU General Public License is available from http://www.gnu.org/ or
** the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
** MA 02111-1307, USA.
**
** For information on splint: info@splint.org
** To report a bug: splint-bug@splint.org
** For more information: http://www.splint.org
*/

# include "splintMacros.nf"
# include "basic.h"

bool firstWord (const char *s, const char *w)
{
  llassert (s != NULL);
  llassert (w != NULL);
  
  for (; *w != '\0'; w++, s++)
    {
      if (*w != *s || *s == '\0')
	return FALSE;
    }
  return TRUE;
}

void
fputline (FILE *out, char *s)
{
  if (strlen (s) > 0) 
    {
      check (fputs (s, out) != EOF);
    }

  check (fputc ('\n', out) == (int) '\n');
}

unsigned int int_toNonNegative (int x) /*@*/
{
  llassert (x >= 0);
  return (unsigned) x;
}

int int_log (int x)
{
  int ret = 1;

  while (x > 10)
    {
      ret++;
      x /= 10;
    }

  return ret;
}

/*@-czechfcns@*/
# ifdef DEADCODE
long unsigned int 
longUnsigned_fromInt (int x)
{
  llassert (x >= 0);
  
  return (long unsigned) x;
}
# endif /* DEADCODE */

size_t size_fromInt (int x) /*@ensures result==x@*/
{
  size_t res = (size_t) x;

  llassert ((int) res == x);
  return res;
}

# ifdef DEADCODE
size_t size_fromLong (long x) /*@ensures result==x@*/
{
  size_t res = (size_t) x;

  llassert ((long) res == x);
  return res;
}
# endif /* DEADCODE */

size_t size_fromLongUnsigned (unsigned long x) /*@ensures result==x@*/
{
  size_t res = (size_t) x;

  llassert ((unsigned long) res == x);
  return res;
}

int size_toInt (size_t x)
{
  int res = (int) x;

  llassert ((size_t) res == x);
  return res;
}

long size_toLong (size_t x)
{
  long res = (long) x;

  llassert ((size_t) res == x);
  return res;
}

char
char_fromInt (int x)
{
  /*
  ** evans 2001-09-28 - changed assertion in response to Anthony Giorgio's comment 
  ** that the old assertions failed for EBCDIC character set.  Now we just check 
  ** that the result is equal.
  */

  char res = (char) x;
  llassert ((int) res == x);
  return res;
}

int
longUnsigned_toInt (long unsigned int x)
{
  int res = (int) x;

  llassert ((long unsigned) res == x);
  return res;
}

int
long_toInt (long int x)
{
  int res = (int) x;

  /*@+ignorequals@*/ llassert (res == x); /*@=ignorequals@*/
  return res;
}

long unsigned size_toLongUnsigned (size_t x)
{
  long unsigned res = (long unsigned) x;

  llassert ((size_t) res == x);
  return res;
}

/*@+czechfcns@*/


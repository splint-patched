/*
** Copyright (C) University of Virginia, Massachusetts Institue of Technology 1994-2003.
** See ../LICENSE for license information.
**
*/

# ifndef FCNNODE_H
# define FCNNODE_H

typedef struct {
  ltoken name;
  /*@null@*/ lclTypeSpecNode typespec;
  declaratorNode declarator;
  globalList globals;
  varDeclarationNodeList inits;
  letDeclNodeList lets;
  /*@null@*/ lclPredicateNode checks;
  /*@null@*/ lclPredicateNode require;
  /*@null@*/ modifyNode modify;
  /*@null@*/ lclPredicateNode ensures;
  /*@null@*/ lclPredicateNode claim;
  qual special;
} *fcnNode;

extern void fcnNode_free (/*@null@*/ /*@only@*/ fcnNode p_f);

# else
# error "Multiple include"
# endif


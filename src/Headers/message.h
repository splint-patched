/*
** Copyright (C) University of Virginia, Massachusetts Institue of Technology 1994-2003.
** See ../LICENSE for license information.
**
*/
# ifndef MESSAGE_H
# define MESSAGE_H

/*@messagelike@*/
extern /*@only@*/ cstring message(/*@temp@*/ const char *p_fmt, ...) /*@*/ ;

# else
# error "Multiple include"
# endif

/* ;-*-C-*-;
** vim: set syntax=c filetype=c shiftwidth=2 softtabstop=2 expandtab:
*/

/* 
** Copyright (C) University of Virginia, Massachusetts Institue of Technology 1994-2003.
** See ../LICENSE for license information.
*/

# ifndef SPLINTMACROS_H
# define SPLINTMACROS_H

/*
** This file contains notfunction macros (hence, the .nf extension to
** prevent it being skipped when +neverinclude is used.)
*/


/*
** Compiler support.
*/

#if defined(__clang__)
    /* Clang defines __GNUC__, thus must have this test prior to GCC's */
#   define HAVE_ATTRIBUTE_NORETURN 1
#elif defined(__ICC) || defined(__INTEL_COMPILER)
    /* Intel compiler also defines __GNUC__, thus must have this test prior to GCC's */
#elif defined(__GNUC__) || defined(__GNUG__)
#   define HAVE_ATTRIBUTE_NORETURN 1
#endif

#if defined(HAVE_ATTRIBUTE_NORETURN)
/*
 * From GCC's manual:
 *
 * A few standard library functions, such as abort and exit, cannot return. GCC
 * knows this automatically. Some programs define their own functions that never
 * return. You can declare them noreturn to tell the compiler this fact. [...]
 *
 * The noreturn keyword tells the compiler to assume that `fatal` (the exemple
 * function) cannot return. It can then optimize without regard to what would
 * happen if fatal ever did return. This makes slightly better code. More
 * importantly, it helps avoid spurious warnings of uninitialized variables.
 *
 * The noreturn keyword does not affect the exceptional path when that applies:
 * a noreturn-marked function may still return to the caller by throwing an
 * exception or calling longjmp.
 *
 * Do not assume that registers saved by the calling function are restored
 * before calling the noreturn function.
 *
 * It does not make sense for a noreturn function to have a return type other
 * than void.
 */
#   define NORETURN                 __attribute__((__noreturn__))
#else
#   define NORETURN                 /* unimplemented */
#endif


/*@-namechecks@*/
/*@notfunction@*/
# define abst_typedef typedef /*@abstract@*/ 

/*@notfunction@*/
# define immut_typedef typedef /*@abstract@*/ /*@immutable@*/

/*@=namechecks@*/

/*
** SunOS4 can't handle bit fields correctly.
*/

# ifdef SYSSunOS
/*@notfunction@*/
# define BOOLBITS
# else 
/*@notfunction@*/
# define BOOLBITS : 1
# endif

/*@notfunction@*/
# define NOALIAS(s,t) (/*@ignore@*/ (s == NULL) || (s != t) /*@end@*/)

/*@notfunction@*/
# define INTCOMPARERETURN(x,y) \
   do { if ((x) > (y)) { return 1; } \
        else { if ((x) < (y)) { return -1; }}} while (FALSE);

/*@notfunction@*/
# define COMPARERETURN(z) \
   do { if (z != 0) { return z; } } while (FALSE);

# else
# error "Multiple include"
# endif


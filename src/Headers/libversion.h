/*
** Copyright (C) University of Virginia, Massachusetts Institue of Technology 1994-2003.
** See ../LICENSE for license information.
*/
/*
**  libversion.h
*/

# ifndef LIBVERSION_H
# define LIBVERSION_H

/*
** Minimum version with compatible libraries.
*/

/*@constant float LIBVERSION; @*/
# define LIBVERSION 3.018

# else
# error "Multiple include"
# endif


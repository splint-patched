/* Definitions for CPP library.
   Copyright (C) 1995, 1996, 1997 Free Software Foundation, Inc.
   Written by Per Bothner, 1994-95.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2, or (at your option) any
later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

 In other words, you are welcome to use, share and improve this program.
 You are forbidden to forbid anyone else to use, share and improve
 what you give them.   Help stamp out software-hoarding!  */

# ifndef CPPCONF_H
# define CPPCONF_H

/* We're using CPP library from GCC, which expects (its) configure to set
 * some definition depending on the host/target the compiler is going to be
 * built for. We don't have the distinction between host/target, but we keep,
 * for compatibility, the distinction.
 * Should use (splint's) configure to set these -- these are just guesses!
 */ 

/*@constant int BITS_PER_UNIT = 8@*/
# define BITS_PER_UNIT 8

/*@constant size_t BITS_PER_CHAR@*/
# define BITS_PER_CHAR 8

/*@constant size_t BITS_PER_WORD@*/
# define BITS_PER_WORD 32

/*@constant size_t HOST_BITS_PER_INT@*/
# define HOST_BITS_PER_INT 32

/*@constant size_t HOST_BITS_PER_LONG = 32@*/
# define HOST_BITS_PER_LONG 32

/*@constant char TARGET_BELL@*/
# define TARGET_BELL (char) 6 

/*@constant char TARGET_BS@*/
# define TARGET_BS   (char) 7

/*@constant char TARGET_FF@*/
# define TARGET_FF   (char) 8

/*@constant char TARGET_NEWLINE@*/
# define TARGET_NEWLINE '\n' 

/*@constant char TARGET_CR@*/
# define TARGET_CR '\n'

/*@constant char TARGET_TAB@*/
# define TARGET_TAB '\t'

/*@constant char TARGET_VT@*/
# define TARGET_VT '\v'


#ifndef HOST_BITS_PER_WIDE_INT

#if HOST_BITS_PER_LONG > HOST_BITS_PER_INT
/*@constant int HOST_BITS_PER_WIDE_INT@*/
#define HOST_BITS_PER_WIDE_INT HOST_BITS_PER_LONG
/*@notfunction@*/
#define HOST_WIDE_INT long
#else
/*@constant int HOST_BITS_PER_WIDE_INT@*/
#define HOST_BITS_PER_WIDE_INT HOST_BITS_PER_INT
/*@notfunction@*/
#define HOST_WIDE_INT long
#endif

#endif

#ifndef INT_TYPE_SIZE
/*@constant size_t INT_TYPE_SIZE@*/
#define INT_TYPE_SIZE BITS_PER_WORD
#endif

#ifndef LONG_TYPE_SIZE
/*@constant size_t LONG_TYPE_SIZE@*/
#define LONG_TYPE_SIZE BITS_PER_WORD
#endif

#ifndef WCHAR_TYPE_SIZE
/*@constant size_t WCHAR_TYPE_SIZE@*/
#define WCHAR_TYPE_SIZE INT_TYPE_SIZE
#endif

# ifndef CHAR_TYPE_SIZE
/*@constant size_t CHAR_TYPE_SIZE@*/
# define CHAR_TYPE_SIZE BITS_PER_CHAR
# endif

#ifndef MAX_CHAR_TYPE_SIZE
/*@constant size_t MAX_CHAR_TYPE_SIZE@*/
#define MAX_CHAR_TYPE_SIZE CHAR_TYPE_SIZE
#endif

#ifndef MAX_LONG_TYPE_SIZE
/*@constant size_t MAX_LONG_TYPE_SIZE@*/
#define MAX_LONG_TYPE_SIZE LONG_TYPE_SIZE
#endif

#ifndef MAX_WCHAR_TYPE_SIZE
/*@constant size_t MAX_WCHAR_TYPE_SIZE@*/
#define MAX_WCHAR_TYPE_SIZE WCHAR_TYPE_SIZE
#endif

# else
# error "Multiple include"
# endif


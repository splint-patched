/*
** Copyright (C) University of Virginia, Massachusetts Institue of Technology 1994-2003.
** See ../LICENSE for license information.
**
*/
/*
** cscanner.h
*/

extern int cscanner_input (void) /*@modifies internalState, fileSystem@*/ ;
extern void cscanner_unput (int) /*@modifies internalState, fileSystem@*/ ;

/*
** These are all exported by flex, but not declared:
*/

/* Don't always check cscanner.c */

/*@-declundef@*/
/*@-namechecks@*/
extern char *c_text;
extern size_t c_leng;
extern /*@dependent@*/ /*@open@*/ FILE *c_in;
extern void c_restart (/*@dependent@*/ FILE *);
/*@=namechecks@*/
/*@=declundef@*/

/*
** Copyright (C) University of Virginia, Massachusetts Institue of Technology 1994-2003.
** See ../LICENSE for license information.
**
*/
# ifndef LIBRARY_H
# define LIBRARY_H

extern void dumpState (cstring p_cfname);
extern void loadState (cstring p_cfname);
extern bool loadStandardState (void);

# else
# error "Multiple include"
# endif

# ifndef CPPHASH_H
# define CPPHASH_H

/*@constant int CPP_HASHSIZE@*/
# define CPP_HASHSIZE 1403

/* different kinds of things that can appear in the value field
   of a hash node.  Actually, this may be useless now. */
typedef union u_hashvalue {
  int ival;
  /*@null@*/ /*@owned@*/ char *cpval;
  /*@owned@*/ DEFINITION *defn;
} hashValue;

struct s_hashNode {
  /* double links for easy deletion */
  /*@only@*/ /*@null@*/ hashNode next;
  /*@dependent@*/ /*@null@*/ hashNode prev;

  /*
  ** Also, a back pointer to this node's hash
  ** chain is kept, in case the node is the head
  ** of the chain and gets deleted. 
  */

  /*@null@*/ /*@dependent@*/ hashNode *bucket_hdr;

  enum node_type type;		/* type of special token */
  size_t length;		/* length of token, for quick comparison */
  cstring name;			/* the actual name */
  hashValue value;		/* pointer to expansion, or whatever */
} ;

extern int cpphash_hashCode (const char *p_name, size_t p_len, int p_hashsize) /*@*/ ;

extern /*@exposed@*/ hashNode cpphash_install (const char *, size_t, 
						  enum node_type, int, 
						  /*@only@*/ /*@null@*/ char *, int);

extern void cpphash_installMacro (const char *p_name, size_t p_len, 
		      /*@only@*/ struct definition *p_defn, int p_hash);

extern void cpphash_deleteMacro (/*@exposed@*/ hashNode p_hp) 
     /*@modifies internalState, p_hp@*/;

extern /*@null@*/ /*@exposed@*/ hashNode cpphash_lookup (const char *, size_t, int); 
extern /*@null@*/ /*@exposed@*/ hashNode cpphash_lookupExpand (const char *, size_t, int,
							       bool p_forceExpand); 

extern void cpphash_saveTable (void);
extern void cpphash_restoreTable (void);
extern void cpphash_cleanup (void);

# else
# error "Multiple include"
# endif

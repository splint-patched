/*
** Copyright (C) University of Virginia, Massachusetts Institue of Technology 1994-2003.
** See ../LICENSE for license information.
**
*/
/*
** osd.h
*/

# ifndef OSD_H
# define OSD_H

extern void osd_initMod (void) /*@modifies internalState@*/ ;
extern void osd_destroyMod (void) /*@modifies internalState@*/ ;

typedef enum {
  OSD_FILEFOUND,
  OSD_FILENOTFOUND,
  OSD_PATHTOOLONG 
} filestatus;

extern filestatus   
  osd_getPath (cstring p_path, cstring p_file, /*@out@*/ cstring *p_returnPath)
  /*@modifies *p_returnPath@*/ ;

extern filestatus   
  osd_getExePath (cstring p_path, cstring p_file, /*@out@*/ cstring *p_returnPath)
  /*@modifies *p_returnPath@*/ ;

extern bool osd_fileExists (cstring) /*@*/ ;
extern bool osd_fileIsReadable (cstring) /*@*/;

extern /*@only@*/ cstring osd_absolutePath (cstring p_filename) /*@*/ ;
extern /*@only@*/ cstring osd_outputPath (cstring p_filename) /*@*/ ;
extern /*@exposed@*/ cstring osd_removePreDirs (cstring);

extern bool osd_isConnectChar (char) /*@*/ ;

extern void osd_pathMakePosix (char *pathname);
extern void osd_pathMakeNative (char *pathname);
extern bool osd_pathIsAbsolute (const char* pathname);
extern bool osd_pathHasDir(const char* pathname);

extern char* osd_pathBase (char *pathname);

extern /*@only@*/ cstring osd_pathListConcat (const char*, const char*);
extern bool osd_pathPrefixInPathList (cstring path, cstring pathList);


/*@constant int CALL_SUCCESS@*/
# define CALL_SUCCESS 0
extern int osd_system (cstring p_cmd) /*@modifies fileSystem@*/ ;

extern int osd_getPid (void) ;

extern /*@observer@*/ cstring osd_getCurrentDirectory (void) /*@*/ ;
extern cstring osd_getTempDir (void);

extern int osd_unlink (cstring) /*@modifies fileSystem@*/ ;


typedef enum {
    OSD_MODEREGULAR,
    OSD_MODEDIRECTORY,
    OSD_MODEOTHER
} filemode;

extern int osd_file_mode_and_size (int p_fd, /*@out@*/ filemode *p_mode_pointer,
                                   /*@out@*/ size_t *p_size_pointer);

extern int osd_openForReading (const char *pathname);
extern int osd_readSafe (int desc, char *ptr, int len);
extern int osd_close(int fid);
FILE *osd_createTmpFile (cstring fname, bool read);

extern int osd_fcloseall (void);

# if defined (OS2) || defined (WIN32)

/* Connection string inserted between directory and filename to make a  */
/* full path name.							*/

/*@constant observer char *CONNECTSTR@*/
# define CONNECTSTR	"\\"

/*@constant char CONNECTCHAR@*/
# define CONNECTCHAR	'\\'

/* Directory separator character for search list. */

/*@constant char PATH_SEPARATOR; @*/
# define PATH_SEPARATOR ';'

#else
/* Connection string inserted between directory and filename to make a  */
/* full path name.							*/

/*@constant observer char *CONNECTSTR@*/
# define CONNECTSTR	"/"

/*@constant char CONNECTCHAR; @*/
# define CONNECTCHAR	'/'

/* Directory separator character for search list. */

/*@constant char PATH_SEPARATOR; @*/
# define PATH_SEPARATOR ':'

#endif

# else
# error "Multiple include"
# endif


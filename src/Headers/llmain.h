/*
** Copyright (C) University of Virginia, Massachusetts Institue of Technology 1994-2003.
** See ../LICENSE for license information.
**
*/

# ifndef LLMAIN_H
# define LLMAIN_H

extern /*@noreturn@*/ void failexit (void) NORETURN ;
extern void showHerald (void);

# endif


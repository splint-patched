/*
** Copyright (C) University of Virginia, Massachusetts Institue of Technology 1994-2003.
** See ../LICENSE for license information.
**
*/

# ifndef GENERAL_H
# define GENERAL_H

extern /*@out@*/ /*@only@*/ void * dimalloc  (size_t p_size, const char *p_name, int p_line)
  /*@ensures maxSet(result) == (p_size - 1); @*/ ;
extern /*@notnull@*/ /*@out@*/ /*@only@*/ void * direalloc (/*@returned@*/ /*@only@*/ /*@out@*/ /*@null@*/ void *p_x, 
    size_t p_size, const char *p_name, int p_line);
extern /*@only@*/ void * dicalloc  (size_t p_num, size_t p_size, const char *p_name, int p_line);

/* no file except general.c should use primitive memory operations: */

/*@-exportlocal@*/
# undef malloc
# undef realloc
# undef calloc
# define malloc(s)     (dimalloc(s, __FILE__, __LINE__))
# define calloc(n, s)  (dicalloc(n, s, __FILE__, __LINE__))
# define realloc(v, s) (direalloc(v, s, __FILE__, __LINE__))
/*@=exportlocal@*/

extern void sfree (/*@out@*/ /*@only@*/ /*@null@*/ void *p_x) /*@modifies p_x@*/; 
# define sfree(x) do { if (x != NULL) free(x); } while (0)

extern void sfreeEventually (/*@owned@*/ /*@null@*/ void *p_x) 
   /*@modifies internalState@*/; 


extern /*@only@*/ void * drealloc (/*@special@*/ /*@null@*/ /*@sef@*/ void *p_x, /*@sef@*/ size_t p_size)
  /*@releases p_x@*/ /*@modifies *p_x@*/ ;
extern /*@out@*/ /*@only@*/ void * dmalloc  (/*@sef@*/ size_t p_size) /*@*/
  /*@ensures maxSet(result) == (p_size - 1); @*/ ;

/* drl 12/28/01 Work around for self checking */
# ifndef LINTBUFFERCHECK
# define dmalloc(s)    (dimalloc(s, __FILE__, __LINE__))
# define drealloc(s,l) (direalloc(s, l, __FILE__, __LINE__))
# endif


# else
# error "Multiple include"
# endif


/*
** Copyright (C) University of Virginia, Massachusetts Institue of Technology 1994-2003.
** Copyright (C) Mihail Groza 2016-2017.
**
** See ../LICENSE for license information.
**
*/

# ifndef RESERVEDNAMES_H
# define RESERVEDNAMES_H

bool isStdReservedName (cstring p_name) /*@*/;
bool isCPlusPlusReservedName (cstring p_name) /*@*/;

# else
# error "Multiple include"
# endif


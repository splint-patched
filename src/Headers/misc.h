/*
** Copyright (C) University of Virginia, Massachusetts Institue of Technology 1994-2003.
** See ../LICENSE for license information.
**
*/
/*
** misc.h
**
*/

# ifndef MISC_H
# define MISC_H

extern void assertSet (/*@special@*/ /*@sef@*/ /*@unused@*/ void *p_x) 
   /*@sets p_x, *p_x@*/ ;
# define assertSet(x) ;


/*@-czechfcns@*/
extern unsigned int int_toNonNegative (int p_x) /*@*/ /*@ensures result == p_x@*/ ;
extern int size_toInt (size_t p_x) /*@*/ /*@ensures result == p_x@*/ ;
extern long size_toLong (size_t p_x) /*@*/ /*@ensures result == p_x@*/ ;
extern size_t size_fromInt (int p_x) /*@*/ /*@ensures result == p_x@*/ ;
extern size_t size_fromLongUnsigned (long unsigned p_x) /*@*/ /*@ensures result == p_x@*/ ;
extern int longUnsigned_toInt (long unsigned int p_x) /*@*/ /*@ensures result == p_x@*/ ;
extern int long_toInt (long p_x) /*@*/ /*@ensures result == p_x@*/ ;
extern long unsigned size_toLongUnsigned (size_t p_x) /*@*/  /*@ensures result == p_x@*/ ;
extern char char_fromInt (int p_x) /*@*/ ;
extern int int_log (int p_x) /*@*/ ;
/*@=czechfcns@*/

extern int int_compare (/*@sef@*/ int p_x, /*@sef@*/ int p_y) /*@*/ ;
# define int_compare(x,y) (((x) > (y)) ? 1 : (((x) < (y)) ? -1 : 0))

/*@-macroparams@*/
/*@-macrofcndecl@*/ /* works for lots of types */
# define generic_compare(x,y) (((x) > (y)) ? 1 : (((x) < (y)) ? -1 : 0)) 
/*@=macrofcndecl@*/
/*@=macroparams@*/

# else
# error "Multiple include"
# endif


# ifndef CPP_H
# define CPP_H

extern int cppProcess (/*@dependent@*/ cstring p_infile,
		       /*@dependent@*/ cstring p_outfile);

extern void cppReader_initMod (cstringList *p_cppArgs) /*@modifies internalState@*/ ;
extern void cppReader_destroyMod (void) /*@modifies internalState@*/ ;

# else 
# error "Multiple include"
# endif

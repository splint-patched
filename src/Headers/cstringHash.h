/*
** Copyright (C) University of Virginia, Massachusetts Institue of Technology 1994-2003.
** See ../LICENSE for license information.
**
*/
# ifndef CSTRINGHASH_H
# define CSTRINGHASH_H

extern unsigned int cstring_hashValue (cstring key);

# else
# error "Multiple include"
# endif


/*
** Splint - annotation-assisted static program checker
** Copyright (C) 1994-2003 University of Virginia,
**         Massachusetts Institute of Technology
**
** This program is free software; you can redistribute it and/or modify it
** under the terms of the GNU General Public License as published by the
** Free Software Foundation; either version 2 of the License, or (at your
** option) any later version.
** 
** This program is distributed in the hope that it will be useful, but
** WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** General Public License for more details.
** 
** The GNU General Public License is available from http://www.gnu.org/ or
** the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
** MA 02111-1307, USA.
**
** For information on splint: info@splint.org
** To report a bug: splint-bug@splint.org
** For more information: http://www.splint.org
*/
/*
** cppmain.c
*/
/* CPP main program, using CPP Library.
   Copyright (C) 1995 Free Software Foundation, Inc.
   Written by Per Bothner, 1994-95.

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2, or (at your option) any
later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

 In other words, you are welcome to use, share and improve this program.
 You are forbidden to forbid anyone else to use, share and improve
 what you give them.   Help stamp out software-hoarding!  */

# include "splintMacros.nf"
# include "basic.h"
# include "cpplib.h"
# include "cpphash.h"
# include "cpp.h"


/* char *progname; */

static cppReader s_cppState;

void cppReader_initMod (cstringList *cppArgs)
{
  cpplib_initializeReader (&s_cppState, cppArgs);

  if (!context_getFlag (FLG_SINGLEINCLUDE))
    {
      cpphash_saveTable ();
    }
}

void cppReader_destroyMod (void) 
  /*@globals killed s_cppState@*/
{
  if (CPPOPTIONS (&s_cppState) != NULL) {
    cppCleanup (&s_cppState);
  }
}

int cppProcess (/*@dependent@*/ cstring infile, 
		/*@dependent@*/ cstring outfile) 
{
  FILE *ofile;
  struct cppOptions *opts = CPPOPTIONS (&s_cppState);
  
  llassert (opts != NULL);

  opts->out_fname = outfile;
  opts->in_fname = infile;
  opts->out_fname = outfile;
  
  if (cpplib_fatalErrors (&s_cppState))
    {
      failexit ();
    }
  
  s_cppState.show_column = TRUE;

  if (cppReader_startProcess (&s_cppState, opts->in_fname) == 0) 
    {
      failexit ();
    }

  ofile = fileTable_createTempFile (context_fileTable (), outfile, FALSE);
  
  if (ofile == NULL) 
    {
      fileTable_noDelete (context_fileTable (), outfile);
      llfatalerror (message ("Cannot create temporary file for "
			     "pre-processor output.  Trying to "
			     "open: %s.  Use -tmpdir to change "
			     "the directory for temporary files.",
			     outfile));
    }
  
  for (;;)
    {
      enum cpp_token kind;
      
      llassert (s_cppState.token_buffer != NULL);

      if (!opts->no_output)
	{
	  DPRINTF (("Writing: %s", cstring_copyLength (s_cppState.token_buffer, cpplib_getWritten (&s_cppState))));

	  (void) fwrite (s_cppState.token_buffer, (size_t) 1,
			 cpplib_getWritten (&s_cppState), ofile);
	}
      
      cppReader_setWritten (&s_cppState, 0);
      kind = cpplib_getToken (&s_cppState);
      
      if (kind == CPP_EOF)
	break;
    }

  cppReader_finish (&s_cppState);
  check (fileTable_closeFile (context_fileTable (), ofile));

  /* Restore the original definition table. */

  if (!context_getFlag (FLG_SINGLEINCLUDE))
    {
      cpphash_restoreTable ();  
    }

  
  /* Undefine everything from this file! */

  if (s_cppState.errors != 0) {
    return -1;
  }

  return 0;
}


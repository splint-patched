## Process this file with automake to create Makefile.in. -*-Makefile-*-
##
## Makefile for Splint 3.1.2
## For more information: http://www.splint.org
##
## Copyright (C) 2001-7 University of Virginia,
##         Massachusetts Institute of Technology
##
## Copyright (C) 2016 Mihail Groza
##
## This program is free software; you can redistribute it and/or modify it
## under the terms of the GNU General Public License as published by the
## Free Software Foundation; either version 2 of the License, or (at your
## option) any later version.
## 
## This program is distributed in the hope that it will be useful, but
## WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## The GNU General Public License is available from http://www.gnu.org/ or
## the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston,
## MA 02111-1307, USA.
##
## For information on spint: info@splint.org
## To report a bug: splint-bug@splint.org
## 

AUTOMAKE_OPTIONS = 1.5 foreign

SUBDIRS =  src lib imports doc

EXTRA_DIST = install.html

EXTRA_DIST += test

## avoid conflict with possible redefinition by autotools of check-local
## in a simple fashion, adding an (additional) dependence
check-local: check-local-test
check-local-test:
	@SPLINT=$(abs_top_builddir)/src/splint$(EXEEXT) \
	       LARCH_PATH="$(abs_top_builddir)/lib:$(abs_top_srcdir)/lib" \
	       LCLIMPORTDIR="$(abs_top_builddir)/imports" \
	       $(MAKE) -C $(srcdir)/test

## avoid conflict with possible redefinition by autotools of clean-local
## in a simple fashion, adding an (additional) dependence
clean-local: clean-local-test
clean-local-test:
	@$(MAKE) -C $(srcdir)/test clean

## avoid conflict with possible redefinition by autotools of dist-hook
## in a simple fashion, adding an (additional) dependence
dist-hook: dist-hook-test
dist-hook-test:
	@rm -rf $(distdir)/test/UNUSED
	-@find $(distdir)/test -name '*~' -delete
	-@find $(distdir)/test -name '.*.swp' -delete
	-@find $(distdir)/test -name '.*.swo' -delete
	@$(MAKE) -C $(distdir)/test clean


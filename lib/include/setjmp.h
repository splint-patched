/*
** setjmp.h
*/

typedef /*@abstract@*/ /*@mutable@*/ void *jmp_buf;

int setjmp (/*@out@*/ jmp_buf env) /*@modifies env@*/ ;

/*@mayexit@*/ void longjmp (jmp_buf env, int val) /*@*/ ;

# ifdef POSIX
typedef /*@abstract@*/ /*@mutable@*/ void *sigjmp_buf;

/*@mayexit@*/
void siglongjmp (sigjmp_buf env, int val) /*@*/;

int sigsetjmp (/*@out@*/ sigjmp_buf env, int savemask) /*@modifies env@*/;
# endif /* POSIX */

/*
** wctype.h (added by Amendment 1 to ISO)
*/

# ifdef STRICT
/*@notfunction@*/
#  define SBOOLINT _Bool           
/*@notfunction@*/
#  define SWINTINT wint_t
# else
/*@notfunction@*/
#  define SBOOLINT _Bool /*@alt int@*/
/*@notfunction@*/
#  define SWINTINT wint_t /*@alt int@*/
# endif

SBOOLINT iswalnum (wint_t c) /*@*/ ;
SBOOLINT iswalpha (wint_t c) /*@*/ ;
SBOOLINT iswcntrl (wint_t c) /*@*/ ;
SBOOLINT iswctype (wint_t c, wctype_t ctg) /*@*/ ;
SBOOLINT iswdigit (wint_t c) /*@*/ ;
SBOOLINT iswgraph (wint_t c) /*@*/ ;
SBOOLINT iswlower (wint_t c) /*@*/ ;
SBOOLINT iswprint (wint_t c) /*@*/ ;
SBOOLINT iswpunct (wint_t c) /*@*/ ;
SBOOLINT iswspace (wint_t c) /*@*/ ;
SBOOLINT iswupper (wint_t c) /*@*/ ;
SBOOLINT iswxdigit (wint_t c) /*@*/ ;

SWINTINT towlower (wint_t c) /*@*/ ;
SWINTINT towupper (wint_t c) /*@*/ ;


/* Warning: not sure about this (maybe abstract?): */
typedef /*@integraltype@*/ wctrans_t;

wctrans_t wctrans (const char *property) /*@*/ ;
SWINTINT towctrans (wint_t c, wctrans_t ctg) /*@*/ ;


# ifdef POSIX
SBOOLINT iswalnum_l (wint_t c, locale_t l) /*@*/ ;
SBOOLINT iswalpha_l (wint_t c, locale_t l) /*@*/ ;
SBOOLINT iswcntrl_l (wint_t c, locale_t l) /*@*/ ;
SBOOLINT iswctype_l (wint_t c, wctype_t ctg, locale_t) /*@*/ ;
SBOOLINT iswdigit_l (wint_t c, locale_t l) /*@*/ ;
SBOOLINT iswgraph_l (wint_t c, locale_t l) /*@*/ ;
SBOOLINT iswlower_l (wint_t c, locale_t l) /*@*/ ;
SBOOLINT iswprint_l (wint_t c, locale_t l) /*@*/ ;
SBOOLINT iswpunct_l (wint_t c, locale_t l) /*@*/ ;
SBOOLINT iswspace_l (wint_t c, locale_t l) /*@*/ ;
SBOOLINT iswupper_l (wint_t c, locale_t l) /*@*/ ;
SBOOLINT iswxdigit_l (wint_t c, locale_t l) /*@*/ ;

SWINTINT towlower_l (wint_t c, locale_t l) /*@*/ ;
SWINTINT towupper_l (wint_t c, locale_t l) /*@*/ ;

wctrans_t wctrans_l (const char *property, locale_t l) /*@*/ ;
SWINTINT towctrans_l (wint_t c, wctrans_t ctg, locale_t l) /*@*/ ;
# endif

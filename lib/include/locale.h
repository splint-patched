/*
** locale.h
*/

struct lconv
{
  char *decimal_point;
  char *thousands_sep;
  char *grouping;
  char *int_curr_symbol;
  char *currency_symbol;
  char *mon_decimal_point;
  char *mon_thousands_sep;
  char *mon_grouping;
  char *positive_sign;
  char *negative_sign;
  char int_frac_digits;
  char frac_digits;
  char p_cs_precedes;
  char p_sep_by_space;
  char n_cs_precedes;
  char n_sep_by_space;
  char p_sign_posn;
  char n_sign_posn;
} ;

/*@constant int LC_ALL;@*/
/*@constant int LC_COLLATE;@*/
/*@constant int LC_CTYPE;@*/
/*@constant int LC_MONETARY;@*/
/*@constant int LC_NUMERIC;@*/
/*@constant int LC_TIME;@*/

/*@observer@*/ /*@null@*/
char *setlocale (int category, /*@null@*/ char *locale) 
  /*@modifies internalState, errno@*/ ;

struct lconv *localeconv (void) /*@*/ ;

# ifdef POSIX
/*@constant int LC_MESSAGES;@*/

/*@constant int LC_ALL_MASK;@*/
/*@constant int LC_COLLATE_MASK;@*/
/*@constant int LC_CTYPE_MASK;@*/
/*@constant int LC_MESSAGES_MASK;@*/
/*@constant int LC_MONETARY_MASK;@*/
/*@constant int LC_NUMERIC_MASK;@*/
/*@constant int LC_TIME_MASK;@*/

typedef /*@abstract@*/ locale_t;

extern locale_t LC_GLOBAL_LOCALE;

locale_t duplocale(locale_t);
void freelocale(locale_t);
locale_t newlocale(int, const char *, locale_t);
locale_t uselocale (locale_t);
# endif

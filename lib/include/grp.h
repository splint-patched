/*
** grp.h
*/

struct group {
  char *gr_name;
  gid_t gr_gid;
  char **gr_mem;
};

/*@observer@*/ /*@null@*/
struct group * getgrgid (gid_t gid)
  /*@modifies errno@*/;

/*@observer@*/ /*@null@*/
struct group *getgrnam (const char *nm)
  /*@modifies errno@*/;


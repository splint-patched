
intmax_t  imaxabs(intmax_t) /*@*/ ;
intmax_t  strtoimax(const char *, char **, int) /*@*/ ;
uintmax_t strtoumax(const char *, char **, int) /*@*/ ;
intmax_t  wcstoimax(const wchar_t *, wchar_t **, int) /*@*/ ;
uintmax_t wcstoumax(const wchar_t *, wchar_t **, int) /*@*/ ;

typedef /*@concrete@*/ struct 
{
  intmax_t quot;
  intmax_t rem;
} imaxdiv_t ;
imaxdiv_t imaxdiv (intmax_t num, intmax_t denom) /*@*/ ;


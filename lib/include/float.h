/*
** float.h
*/

/*
** Note, these are defined by macros, but NOT necessarily
** constants.  They may be used as lvalues.
*/

/*@unchecked@*/ int    DBL_DIG;
/*@unchecked@*/ double DBL_EPSILON;
/*@unchecked@*/ int    DBL_MANT_DIG;
/*@unchecked@*/ double DBL_MAX;
/*@unchecked@*/ int    DBL_MAX_10_EXP;
/*@unchecked@*/ int    DBL_MAX_EXP;
/*@unchecked@*/ double DBL_MIN;
/*@unchecked@*/ int    DBL_MIN_10_EXP;
/*@unchecked@*/ int    DBL_MIN_EXP;

/*@unchecked@*/ int   FLT_DIG;
/*@unchecked@*/ float FLT_EPSILON;
/*@unchecked@*/ int   FLT_MANT_DIG;
/*@unchecked@*/ float FLT_MAX;
/*@unchecked@*/ int   FLT_MAX_10_EXP;
/*@unchecked@*/ int   FLT_MAX_EXP;
/*@unchecked@*/ float FLT_MIN;
/*@unchecked@*/ int   FLT_MIN_10_EXP;
/*@unchecked@*/ int   FLT_MIN_EXP;
/*@constant int FLT_RADIX@*/
/*@unchecked@*/ int   FLT_ROUNDS;

/*@unchecked@*/ int         LDBL_DIG;
/*@unchecked@*/ long double LDBL_EPSILON;
/*@unchecked@*/ int         LDBL_MANT_DIG;
/*@unchecked@*/ long double LDBL_MAX;
/*@unchecked@*/ int         LDBL_MAX_10_EXP;
/*@unchecked@*/ int         LDBL_MAX_EXP;
/*@unchecked@*/ long double LDBL_MIN;
/*@unchecked@*/ int         LDBL_MIN_10_EXP;
/*@unchecked@*/ int         LDBL_MIN_EXP;

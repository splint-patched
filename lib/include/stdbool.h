/*
** stdbool.h
*/

/*@-likelybool@*/
typedef _Bool bool;
/*@=likelybool@*/
/*@constant bool true@*/
/*@constant bool false@*/
/*@constant int __bool_true_false_are_defined = 1@*/


/*
 * poll.h
 */

typedef /*@unsignedintegraltype@*/ nfds_t;

struct pollfd {
  int fd;
  short events;
  short revents;
};

/*@constant short POLLIN@*/
/*@constant short POLLRDNORM@*/
/*@constant short POLLRDBAND@*/
/*@constant short POLLPRI@*/
/*@constant short POLLOUT@*/
/*@constant short POLLWRNORM@*/
/*@constant short POLLWRBAND@*/
/*@constant short POLLERR@*/
/*@constant short POLLHUP@*/
/*@constant short POLLNVAL@*/

int poll (struct pollfd pfd[], nfds_t nfds, int timeout)
  /*@modifies pfd[].revents, errno@*/ ;


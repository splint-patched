/*
** time.h
*/

/*@constant int CLOCKS_PER_SEC;@*/

typedef /*@integraltype@*/ clock_t;
typedef /*@integraltype@*/ time_t;

struct tm
  {
    int tm_sec;
    int tm_min;
    int tm_hour;
    int tm_mday;
    int tm_mon;
    int tm_year;
    int tm_wday;
    int tm_yday;
    int tm_isdst;
  } ;

clock_t clock (void) /*@modifies internalState@*/ ;
double difftime (time_t time1, time_t time0) /*@*/ ;
time_t mktime (struct tm *timeptr) /*@*/ ;

time_t time (/*@null@*/ /*@out@*/ time_t *tp)
  /*@modifies *tp@*/ ;

/*@observer@*/
char *asctime (struct tm *timeptr) 
  /*@modifies errno*/ /*@ensures maxSet(result) == 25 /\ maxRead(result) == 25; @*/ ;

/*@observer@*/
char *ctime (time_t *tp) /*@*/
  /*@ensures maxSet(result) == 25 /\ maxRead(result) == 25; @*/;

/*@observer@*/
struct tm *gmtime (time_t *tp) /*@*/ ;

/*@observer@*/
struct tm *localtime (time_t *tp) 
  /*@modifies errno@*/ ;

size_t strftime (/*@out@*/ char *s, size_t smax,
    char *fmt, struct tm *timeptr)
  /*@modifies *s@*/ ;

# ifdef POSIX
/*@constant int CLK_TCK@*/

/*@unchecked@*/ int daylight;
/*@unchecked@*/ long int timezone;
/*@unchecked@*/ char *tzname[];

void tzset (void)
  /*@globals environ@*/
  /*@modifies daylight, timezone, tzname@*/;

# endif /* POSIX */

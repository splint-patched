
typedef /*@integraltype@*/ wint_t;
typedef /*@abstract@*/ mbstate_t;
/* Warning: not sure about this (maybe abstract?): */
typedef /*@integraltype@*/ wctype_t;

/*@constant wint_t WEOF@*/

wint_t btowc (int c) /*@*/ ;

wint_t fgetwc (FILE *fp) /*@modifies fileSystem, *fp*/ ;

/*@null@*/
wchar_t *fgetws (/*@returned@*/ wchar_t *s, int n, FILE *stream)
  /*@modifies fileSystem, *s, *stream@*/;

wint_t fputwc (wchar_t c, FILE *stream)
  /*@modifies fileSystem, *stream@*/;

int fputws (const wchar_t *s, FILE *stream)
  /*@modifies fileSystem, *stream@*/ ;

/* does not modify the stream */
int fwide (FILE *stream, int mode) /*@*/ ; 

/*@printflike@*/
int fwprintf (FILE *stream, const wchar_t *format, ...)
  /*@modifies *stream, fileSystem@*/ ;

/*@scanflike@*/
int fwscanf (FILE *stream, const wchar_t *format, ...)
  /*@modifies *stream, fileSystem@*/ ;

/* note use of sef --- stream may be evaluated more than once */
wint_t getwc (/*@sef@*/ FILE *stream)
  /*@modifies fileSystem, *stream@*/ ;

wint_t getwchar (void)
  /*@modifies fileSystem, *stdin@*/;

size_t mbrlen (const char *s, size_t n, /*@null@*/ mbstate_t *ps) /*@*/ ;

size_t mbrtowc (/*@null@*/ wchar_t *pwc, const char *s, size_t n,
    /*@null@*/ mbstate_t *ps) 
  /*@modifies *pwc@*/ ;

int mbsinit (/*@null@*/ const mbstate_t *ps) /*@*/ ;

size_t mbsrtowcs (/*@null@*/ wchar_t *dst, const char **src, size_t len,
    /*@null@*/ mbstate_t *ps) 
  /*@modifies *dst, ps@*/ ;

/* note use of sef --- stream may be evaluated more than once */
wint_t putwc (wchar_t c, /*@sef@*/ FILE *stream)
  /*@modifies fileSystem, *stream@*/ ;

wint_t putwchar (wchar_t c)
  /*@modifies fileSystem, *stdout@*/ ;

/*@printflike@*/
int swprintf (wchar_t *s, size_t n, const wchar_t *format, ...)
  /*@modifies *s@*/ ;

/*@scanflike@*/
int swscanf (const wchar_t *s, const wchar_t *format, ...)
  /*@modifies *stdin@*/ ;

wint_t ungetwc (wint_t c, FILE *stream)
  /*@modifies fileSystem, *stream@*/ ;

int vfwprintf (FILE *stream, const wchar_t *format, va_list arg)
  /*@modifies fileSystem, *stream@*/ ;

int vswprintf (wchar_t *s, size_t n, const wchar_t *format, va_list arg)
  /*@modifies *s@*/ ;

int vwprintf (const wchar_t *format, va_list arg)
  /*@modifies fileSystem, *stdout@*/ ;

size_t wcrtomb (/*@null@*/ /*@out@*/ char *s, wchar_t wc, /*@null@*/ mbstate_t *ps)
  /*@modifies *s@*/;

void /*@alt wchar_t *@*/ wcscat (/*@unique@*/ /*@returned@*/ /*@out@*/ wchar_t *s1,
    const wchar_t *s2)
  /*@modifies *s1@*/ ;

/*@exposed@*/ /*@null@*/
wchar_t * wcschr (/*@returned@*/ const wchar_t *s, wchar_t c) /*@*/ ;

int wcscmp (const wchar_t *s1, const wchar_t *s2) /*@*/ ;

int wcscoll (const wchar_t *s1, const wchar_t *s2) /*@*/ ;

void /*@alt wchar_t *@*/ wcscpy (/*@unique@*/ /*@out@*/ /*@returned@*/ wchar_t *s1,
    const wchar_t *s2)
  /*@modifies *s1@*/ ;

size_t wcscspn (const wchar_t *s1, const wchar_t *s2) /*@*/ ;

size_t wcsftime (/*@out@*/ wchar_t *s, size_t maxsize, const wchar_t *format,
    const struct tm *timeptr) 
  /*@modifies *s@*/ ;

size_t wcslen (const wchar_t *s) /*@*/ ;

#ifdef STRICT
wchar_t *
#else
void /*@alt wchar_t *@*/
#endif
wcsncat (/*@unique@*/ /*@returned@*/ /*@out@*/ wchar_t *s1,
    const wchar_t *s2, size_t n) 
  /*@modifies *s1@*/ ;

int wcsncmp (const wchar_t *s1, const wchar_t *s2, size_t n) /*@*/ ;

#ifdef STRICT
wchar_t *
#else
void /*@alt wchar_t *@*/
#endif
wcsncpy (/*@unique@*/ /*@returned@*/ /*@out@*/ wchar_t *s1,
    const wchar_t *s2, size_t n) 
  /*@modifies *s1@*/ ;

/*@null@*/
wchar_t * wcspbrk (/*@returned@*/ const wchar_t *s1, const wchar_t *s2) /*@*/ ;

/*@null@*/
wchar_t * wcsrchr (/*@returned@*/ const wchar_t *s, wchar_t c) /*@*/ ;

size_t wcsrtombs (/*@null@*/ char *dst, const wchar_t **src, size_t len,
	     /*@null@*/ mbstate_t *ps) 
  /*@modifies *src@*/ ;

size_t wcsspn (const wchar_t *s1, const wchar_t *s2) /*@*/ ;

/*@null@*/
wchar_t *wcsstr (const wchar_t *s1, const wchar_t *s2) /*@*/ ;

double wcstod (const wchar_t *nptr, /*@null@*/ wchar_t **endptr)
   /*@modifies *endptr@*/ ;

float wcstof (const wchar_t *nptr, /*@null@*/ wchar_t **endptr)
   /*@modifies *endptr@*/ ;

long double wcstold (const wchar_t *nptr, /*@null@*/ wchar_t **endptr)
   /*@modifies *endptr@*/ ;

/*@null@*/
wchar_t * wcstok (/*@null@*/ wchar_t *s1, const wchar_t *s2, wchar_t **ptr)
  /*@modifies *ptr@*/;

long wcstol (const wchar_t *nptr, /*@null@*/ wchar_t **endptr, int base)
  /*@modifies *endptr@*/;

unsigned long wcstoul (const wchar_t *nptr, /*@null@*/ wchar_t **endptr, int base)
  /*@modifies *endptr@*/;

long long wcstoll (const wchar_t *nptr, /*@null@*/ wchar_t **endptr, int base)
  /*@modifies *endptr@*/;

unsigned long long wcstoull (const wchar_t *nptr, /*@null@*/ wchar_t **endptr, int base)
  /*@modifies *endptr@*/;

size_t wcsxfrm (/*@null@*/ wchar_t *s1, const wchar_t *s2, size_t n)
  /*@modifies *s1@*/;

int wctob (wint_t c) /*@*/;

wctype_t wctype (const char *property) /*@*/ ;

/*@null@*/
wchar_t *wmemchr (const wchar_t *s, wchar_t c, size_t n) /*@*/ ;

int wmemcmp (const wchar_t *s1, const wchar_t *s2, size_t n) /*@*/ ;

wchar_t *wmemcpy (/*@returned@*/ wchar_t *s1, const wchar_t *s2, size_t n)
  /*@modifies *s1@*/;

wchar_t *wmemmove (/*@returned@*/ wchar_t *s1, const wchar_t *s2, size_t n)
  /*@modifies *s1@*/;

wchar_t *wmemset (/*@returned@*/ wchar_t *s, wchar_t c, size_t n)
  /*@modifies *s@*/;

/*@printflike@*/
int wprintf (const wchar_t *format, ...)
  /*@globals stdout@*/
  /*@modifies errno, *stdout@*/;

/*@scanflike@*/
int wscanf (const wchar_t *format, ...)
  /*@globals stdin@*/
  /*@modifies errno, *stdin@*/;

#if 1 /* TODO: add annotations ... */
FILE *open_wmemstream(wchar_t **, size_t *);
int vfwscanf(FILE *, const wchar_t *, va_list);
int vswscanf(const wchar_t *, const wchar_t *,
    va_list);
int vwscanf(const wchar_t *, va_list);
#endif


# ifdef POSIX
size_t mbsnrtowcs(/*@null@*/ wchar_t *dst, const char **src, size_t len,
    size_t nms, /*@null@*/ mbstate_t *ps)
  /*@modifies dst, ps@*/;

/*@null@*/ /*@only@*/
wchar_t *wcsdup(const wchar_t *) /*@*/ ;

int wcscoll_l(const wchar_t *, const wchar_t *, locale_t) /*@*/ ;

size_t wcsxfrm_l (/*@null@*/ wchar_t *s1, const wchar_t *s2, size_t n, locale_t l)
  /*@modifies *s1@*/;

int wcwidth(wchar_t) /*@*/ ;

#if 1 /* TODO: add annotations ... */
wchar_t *wcpcpy(wchar_t *, const wchar_t *);
wchar_t *wcpncpy(wchar_t *, const wchar_t *, size_t);
int wcscasecmp(const wchar_t *, const wchar_t *);
int wcscasecmp_l(const wchar_t *, const wchar_t *, locale_t);
int wcsncasecmp(const wchar_t *, const wchar_t *, size_t);
int wcsncasecmp_l(const wchar_t *, const wchar_t *, size_t,
    locale_t);
size_t wcsnlen(const wchar_t *, size_t);
size_t wcsnrtombs(char *, const wchar_t **, size_t,
    size_t, mbstate_t *);
int wcswidth(const wchar_t *, size_t);
#endif

# endif /* POSIX */

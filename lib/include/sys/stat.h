/*
** sys/stat.h
*/

struct stat {
  dev_t     st_dev; /* ID of device containing file */
  ino_t     st_ino; /* file serial number */
  mode_t    st_mode; /* mode of file (see below) */
  nlink_t   st_nlink; /* number of links to the file */
  uid_t     st_uid; /* user ID of file */
  gid_t     st_gid; /* group ID of file */
  off_t     st_size; /* file size in bytes (if file is a regular file) */
  time_t    st_atime; /* time of last access */
  time_t    st_mtime; /* time of last data modification */
  time_t    st_ctime; /* time of last status change */

# ifdef UNIX /* more required fields in Unix */
  dev_t     st_rdev; /* device ID (if file is character or block special) */
  blksize_t st_blksize; /* a filesystem-specific preferred I/O block size for
			   this object.  In some filesystem types, this may
			   vary from file to file */
  blkcnt_t  st_blocks; /*  number of blocks allocated for this object */
# endif
} ;

/*
** POSIX does not require that the S_I* be functions. They're
** macros virtually everywhere. 
*/

# ifdef STRICT
/*@notfunction@*/
# define SBOOLINT _Bool           
# else
/*@notfunction@*/
# define SBOOLINT _Bool /*@alt int@*/
# endif

SBOOLINT S_ISBLK (/*@sef@*/ mode_t m) /*@*/ ;

SBOOLINT S_ISCHR (/*@sef@*/ mode_t m) /*@*/ ;

SBOOLINT S_ISDIR (/*@sef@*/ mode_t m) /*@*/ ;

SBOOLINT S_ISFIFO (/*@sef@*/ mode_t m) /*@*/ ;

SBOOLINT S_ISREG (/*@sef@*/ mode_t m) /*@*/ ;

int chmod (const char *path, mode_t mode)
  /*@modifies fileSystem, errno@*/ ;
     
int fstat (int fd, /*@out@*/ struct stat *buf)
  /*@modifies errno, *buf@*/ ;
     
int mkdir (const char *path, mode_t mode)
  /*@modifies fileSystem, errno@*/;
     
int mkfifo (const char *path, mode_t mode)
  /*@modifies fileSystem, errno@*/;

int stat (const char *path, /*@out@*/ struct stat *buf)
  /*:errorcode -1*/
  /*@modifies errno, *buf@*/;

int umask (mode_t cmask)
  /*@modifies systemState@*/;


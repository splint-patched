/*
** sys/utsname.h
*/

struct utsname {
  char	sysname[];
  char	nodename[];
  char	release[];
  char	version[];
  char	machine[];
};

int uname (/*@out@*/ struct utsname *name)
  /*@modifies *name, errno@*/ ;


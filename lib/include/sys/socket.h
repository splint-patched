/*
 * sys/socket.h
 */

/*@constant int SCM_RIGHTS@*/

/*@constant int SOCK_DGRAM@*/
/*@constant int SOCK_RAW@*/
/*@constant int SOCK_SEQPACKET@*/
/*@constant int SOCK_STREAM@*/

/*@constant int SOL_SOCKET@*/

/*@constant int SO_ACCEPTCONN@*/
/*@constant int SO_BROADCAST@*/
/*@constant int SO_DEBUG@*/
/*@constant int SO_DONTROUTE@*/
/*@constant int SO_ERROR@*/
/*@constant int SO_KEEPALIVE@*/
/*@constant int SO_LINGER@*/
/*@constant int SO_OOBINLINE@*/
/*@constant int SO_RCVBUF@*/
/*@constant int SO_RCVLOWAT@*/
/*@constant int SO_RCVTIMEO@*/
/*@constant int SO_REUSEADDR@*/
/*@constant int SO_SNDBUF@*/
/*@constant int SO_SNDLOWAT@*/
/*@constant int SO_SNDTIMEO@*/
/*@constant int SO_TYPE@*/

/*@constant int SOMAXCONN@*/

/*@constant int MSG_CTRUNC@*/
/*@constant int MSG_DONTROUTE@*/
/*@constant int MSG_EOR@*/
/*@constant int MSG_OOB@*/
/*@constant int MSG_NOSIGNAL@*/
/*@constant int MSG_PEEK@*/
/*@constant int MSG_TRUNC@*/
/*@constant int MSG_WAITALL@*/

/*@constant int AF_INET@*/
/*@constant int AF_INET6@*/
/*@constant int AF_UNIX@*/
/*@constant int AF_UNSPEC = 0;@*/

/*@constant int SHUT_RD@*/
/*@constant int SHUT_RDWR@*/
/*@constant int SHUT_WR@*/


typedef /*@unsignedintegraltype@*/ socklen_t;

typedef /*@unsignedintegraltype@*/ sa_family_t;

struct sockaddr {
  sa_family_t	sa_family;		/* address family */
  char          sa_data[];		/* variable length */
};

struct sockaddr_storage {
  sa_family_t ss_family;
} ;

struct msghdr {
  /*@dependent@*/ void *msg_name;		
  socklen_t msg_namelen;	/*: maxSet (msg_name) >= msg_namelen */
  /*@dependent@*/ struct iovec *msg_iov;	/* scatter/gather array */
  int msg_iovlen;		/* # elements in msg_iov */ /*: maxSet (msg_iov) >= msg_iovlen */
  /*@dependent@*/ void *msg_control;		/* ancillary data, see below */
  socklen_t msg_controllen;     /*: maxSet (msg_control) >= msg_controllen */
  int msg_flags;		/* flags on received message */
} ;

struct cmsghdr {
  socklen_t cmsg_len;		/* data byte count, including hdr */
  int cmsg_level;		/* originating protocol */
  int cmsg_type;		/* protocol-specific type */
} ;

/*@exposed@*/ unsigned char *CMSG_DATA (/*@sef@*/ struct cmsghdr *) /*@*/ ;
/*@null@*/ /*@exposed@*/ struct cmsghdr *CMSG_NXTHDR (struct msghdr *, struct cmsghdr *) /*@*/ ;
/*@null@*/ /*@exposed@*/ struct cmsghdr *CMSG_FIRSTHDR (struct msghdr *) /*@*/ ;

struct linger {
  int l_onoff;	
  int l_linger;	
};

int accept (int s, struct sockaddr *addr, int *addrlen)
  /*@modifies *addrlen, errno@*/;

int bind (int s, const struct sockaddr *name, int namelen)
  /*@modifies errno, fileSystem@*/;

int connect (int s, const struct sockaddr *name, int namelen)
  /*@modifies errno, internalState@*/;

int getpeername (int s, /*@out@*/ struct sockaddr * restrict name, socklen_t * restrict namelen)
   /*@modifies *name, *namelen, errno@*/;
	
int getsockname (int s, /*@out@*/ struct sockaddr *address, socklen_t
#ifndef STRICT
    /*@alt size_t@*/
#endif
    *address_len)
  /*: can't do this? requires maxSet(address) >= (*address_len) @*/ 
  /*@modifies *address, *address_len, errno@*/;
  
int getsockopt (int s, int level, int optname, /*@out@*/ void *optval, size_t *optlen)
	/*@modifies *optval, *optlen, errno@*/;

int listen (int s, int backlog)
  /*@modifies errno, internalState@*/;

ssize_t recv (int s, /*@out@*/ void *buf, size_t len, int flags)
  /*@modifies *buf, errno@*/;

ssize_t recvfrom (int s, void *buf, size_t len, int flags,
    /*@null@*/ struct sockaddr *from, int *fromlen)
  /*@modifies *buf, *from, *fromlen, errno@*/;

ssize_t recvmsg (int s, struct msghdr *msg, int flags)
  /*@modifies msg->msg_iov->iov_base[], errno@*/;

ssize_t send (int s, const void *msg, size_t len, int flags)
  /*@modifies errno@*/;

ssize_t sendto (int s, const void *msg, size_t len, int flags,
    const struct sockaddr *to, int tolen)
  /*@modifies errno@*/;

ssize_t sendmsg (int s, const struct msghdr *msg, int flags)
  /*@modifies errno@*/;

int setsockopt (int s, int level, int optname, const void *optval, int optlen)
  /*@modifies internalState, errno@*/;

int shutdown (int s, int how)
  /*@modifies errno@*/;

int socket (int domain, int type, int protocol)
  /*@modifies errno@*/;

int socketpair (int d, int type, int protocol, /*@out@*/ int *sv)
  /*@modifies errno@*/;


# ifdef UNIX
/*@constant int SOCK_RDM@*/

/*@constant int SO_USELOOPBACK@*/
/*@constant int SO_REUSEPORT@*/

/*@constant int AF_LOCAL@*/
/*@constant int AF_IMPLINK@*/
/*@constant int AF_PUP@*/
/*@constant int AF_CHAOS@*/
/*@constant int AF_NS@*/
/*@constant int AF_ISO@*/
/*@constant int AF_OSI@*/
/*@constant int AF_ECMA@*/
/*@constant int AF_DATAKIT@*/
/*@constant int AF_CCITT@*/
/*@constant int AF_SNA@*/
/*@constant int AF_DECnet@*/
/*@constant int AF_DLI@*/
/*@constant int AF_LAT@*/
/*@constant int AF_HYLINK@*/
/*@constant int AF_APPLETALK@*/
/*@constant int AF_ROUTE@*/
/*@constant int AF_LINK@*/
/*@constant int pseudo_AF_XTP@*/
/*@constant int AF_COIP@*/
/*@constant int AF_CNT@*/
/*@constant int pseudo_AF_RTIP@*/
/*@constant int AF_IPX@*/
/*@constant int AF_SIP@*/
/*@constant int pseudo_AF_PIP@*/
/*@constant int AF_ISDN@*/
/*@constant int AF_E164@*/
/*@constant int AF_MAX@*/

/*@constant int MSG_DONTWAIT@*/
/*@constant int MSG_EOF@*/
/*@constant int MSG_COMPAT@*/

/*@constant int PF_UNSPEC@*/
/*@constant int PF_LOCAL@*/
/*@constant int PF_UNIX@*/
/*@constant int PF_INET@*/
/*@constant int PF_IMPLINK@*/
/*@constant int PF_PUP@*/
/*@constant int PF_CHAOS@*/
/*@constant int PF_NS@*/
/*@constant int PF_ISO@*/
/*@constant int PF_OSI@*/
/*@constant int PF_ECMA@*/
/*@constant int PF_DATAKIT@*/
/*@constant int PF_CCITT@*/
/*@constant int PF_SNA@*/
/*@constant int PF_DECnet@*/
/*@constant int PF_DLI@*/
/*@constant int PF_LAT@*/
/*@constant int PF_HYLINK@*/
/*@constant int PF_APPLETALK@*/
/*@constant int PF_ROUTE@*/
/*@constant int PF_LINK@*/
/*@constant int PF_XTP@*/
/*@constant int PF_COIP@*/
/*@constant int PF_CNT@*/
/*@constant int PF_SIP@*/
/*@constant int PF_IPX@*/
/*@constant int PF_RTIP@*/
/*@constant int PF_PIP@*/
/*@constant int PF_ISDN@*/
/*@constant int PF_MAX@*/

/*@constant int NET_MAXID@*/
/*@constant int NET_RT_DUMP@*/
/*@constant int NET_RT_FLAGS@*/
/*@constant int NET_RT_IFLIST@*/
/*@constant int NET_RT_MAXID@*/

typedef /*@unsignedintegraltype@*/ __socklen_t; /* needed by linux */

# if 0 /* These were in the old unix.h spec, but are not in SUS6 */
struct sockproto {
  u_short	sp_family;		/* address family */
  u_short	sp_protocol;		/* protocol */
};
# endif

# endif

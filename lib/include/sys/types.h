/*
** sys/types.h
*/

typedef /*@integraltype@*/ dev_t;
typedef /*@integraltype@*/ gid_t;
typedef /*@unsignedintegraltype@*/ ino_t; /*: is this definitely unsigned? */
typedef /*@integraltype@*/ mode_t;
typedef /*@integraltype@*/ nlink_t;
typedef /*@integraltype@*/ off_t;
typedef /*@integraltype@*/ pid_t;
typedef /*@integraltype@*/ uid_t;

typedef /*@signedintegraltype@*/ suseconds_t;
typedef /*@unsignedintegraltype@*/ useconds_t;

# ifdef UNIX
typedef /*@integraltype@*/ blkcnt_t;
typedef /*@integraltype@*/ blksize_t;

typedef /*@integraltype@*/ clockid_t;
typedef /*@unsignedintegraltype@*/ fsblkcnt_t;
typedef /*@unsignedintegraltype@*/ fsfilcnt_t;
typedef /*@integraltype@*/ id_t;

typedef /*@integraltype@*/ key_t;
typedef /*@integraltype@*/ timer_t;

typedef /*@integraltype@*/ pthread_attr_t;
typedef /*@integraltype@*/ pthread_cond_t;
typedef /*@integraltype@*/ pthread_condattr_t;
typedef /*@integraltype@*/ pthread_key_t;
typedef /*@integraltype@*/ pthread_mutex_t;
typedef /*@integraltype@*/ pthread_mutexattr_t;
typedef /*@integraltype@*/ pthread_once_t;
typedef /*@integraltype@*/ pthread_rwlock_t;
typedef /*@integraltype@*/ pthread_rwlockattr_t;
typedef /*@integraltype@*/ pthread_t;
# endif


/*
** sys/uio.h
*/

struct iovec {
  /*@dependent@*/ void *iov_base;
  size_t iov_len; /*: maxSet(iov_base) = iov_len */
};

ssize_t readv (int fd, const struct iovec *iov, int iovcnt)
     /*@modifies iov->iov_base, fileSystem, errno@*/;

ssize_t writev (int fd, const struct iovec *iov, int iovcnt)
     /*@modifies errno@*/;


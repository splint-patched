/*
** sys/wait.h
*/

/*@constant int WNOHANG@*/
/*@constant int WUNTRACED@*/
/*@constant int WCONTINUED@*/

int WEXITSTATUS (int status) /*@*/ ;
int WIFEXITED (int status) /*@*/ ;
int WIFSIGNALED (int status) /*@*/ ;
int WIFSTOPPED (int status) /*@*/ ;
int WSTOPSIG (int status) /*@*/ ;
int WTERMSIG (int status) /*@*/ ;

pid_t wait (/*@out@*/ /*@null@*/ int *st)
   /*@modifies *st, errno, systemState@*/;

pid_t waitpid (pid_t pid, /*@out@*/ /*@null@*/ int *st, int opt)
   /*@modifies *st, errno, systemState@*/;

# ifdef UNIX

/*@constant int WAIT_ANY@*/
/*@constant int WAIT_MYPGRP@*/
/*@constant int WSTOPPED@*/

int WCOREDUMP (int x) /*@*/;

int W_EXITCODE (int ret, int sig) /*@*/;

int W_STOPCODE (int sig) /*@*/;

pid_t wait3 (int *statloc, int options, /*@null@*/ /*@out@*/ struct rusage *rusage)
  /*@modifies *statloc, *rusage, errno@*/;

pid_t wait4 (pid_t p, int *statloc, int opt, /*@null@*/ /*@out@*/ struct rusage *r)
  /*@modifies *statloc, *r, errno@*/;

# endif /* UNIX */


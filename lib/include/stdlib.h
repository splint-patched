/*
** stdlib.h
*/

/*@constant int EXIT_FAILURE; @*/ 
/*@constant int EXIT_SUCCESS; @*/ 

/*@constant int RAND_MAX; @*/

/*@constant size_t MB_CUR_MAX; @*/

double atof (char *s) /*@*/ ;

int atoi (char *s) /*@*/ ;

long int atol (char *s) /*@*/ ;

float strtof (const char char *s, /*@null@*/ /*@out@*/ char ** endp)
  /*@modifies *endp, errno@*/ ;

double strtod (const char char *s, /*@null@*/ /*@out@*/ char ** endp)
  /*@modifies *endp, errno@*/ ;

long double strtold (const char char *s, /*@null@*/ /*@out@*/ char ** endp)
  /*@modifies *endp, errno@*/ ;

long strtol (const char char *s, /*@null@*/ /*@out@*/ char **endp, int base)
  /*@modifies *endp, errno@*/ ;

unsigned long strtoul (const char *s, /*@null@*/ /*@out@*/ char **endp, int base)
  /*@modifies *endp, errno@*/ ;

long long strtoll (const char char *s, /*@null@*/ /*@out@*/ char **endp, int base)
  /*@modifies *endp, errno@*/ ;

unsigned long long strtoull (const char *s, /*@null@*/ /*@out@*/ char **endp, int base)
  /*@modifies *endp, errno@*/ ;

int rand (void)
  /*@modifies internalState@*/ ;

void srand (unsigned int seed)
  /*@modifies internalState@*/ ;

/*@null@*/ /*@out@*/ /*@only@*/
void *calloc (size_t nobj, size_t size)
# ifdef POSIX
  /*@modifies errno@*/
#else
  /*@*/
# endif
  /*@ensures maxSet(result) == (nobj - 1) @*/ ;

/*@null@*/ /*@out@*/ /*@only@*/
void *malloc (size_t size)
# ifdef POSIX
  /*@modifies errno@*/
#else
  /*@*/
#endif
  /*@ensures maxSet(result) == (size - 1) @*/ ;

/*
** SPLint annotations cannot fully describe realloc.  The semantics we
** want are:
**    realloc returns null: ownership of parameter is not changed
**    realloc returns non-null: ownership of parameter is transferred to return value
**
** Otherwise, storage is in the same state before and after the call.
*/

/*@null@*/ /*@only@*/
void * realloc (/*@null@*/ /*@only@*/ /*@out@*/ /*@returned@*/ void *p, size_t size) 
# ifdef POSIX
  /*@modifies *p, errno@*/
#else
  /*@modifies *p@*/
#endif
  /*@ensures maxSet(result) >= (size - 1) @*/;

void free (
# ifdef UNIX /* free does not take null */
    /*@notnull@*/
# else
    /*@null@*/
# endif
    /*@out@*/ /*@only@*/ void *p) /*@modifies p@*/ ;

/*@exits@*/ void abort (void) /*@*/ ;

/*@exits@*/ void exit (int status) /*@*/ ;

int atexit (void (*func)(void)) /*@modifies internalState@*/ ;

/*@observer@*/ /*@null@*/ char *getenv (char *name) /*@*/ ;

int system (/*@null@*/ const char *s)
  /*@modifies fileSystem, errno@*/ ;

/*@null@*/ /*@dependent@*/
void * bsearch (const void *key, const void *base, 
                size_t n, size_t size, 
                int (*compar)(const void *, const void *))
  /*@*/
#if UNIX
  /*@requires maxSet(base) >= (n - 1) @*/
#endif
  ;

void qsort (void *base, size_t n, size_t size,
    int (*compar)(const void *, const void *) )
  /*@requires maxRead(base) >= (n - 1) @*/
  /*@modifies *base, errno@*/ ;

int abs (int n) /*@*/ ;

long int labs (long int n) /*@*/ ; 

typedef /*@concrete@*/ struct 
{
  int quot;
  int rem;
} div_t ;
div_t div (int num, int denom) /*@*/ ;

typedef /*@concrete@*/ struct 
{
  long int quot;
  long int rem;
} ldiv_t ;
ldiv_t ldiv (long int num, long int denom) /*@*/ ;

typedef /*@concrete@*/ struct 
{
  long long int quot;
  long long int rem;
} lldiv_t ;
lldiv_t lldiv (long long int num, long long int denom) /*@*/ ;

int mblen (char *s, size_t n) /*@*/ ;
int mbtowc (/*@null@*/ wchar_t *pwc, /*@null@*/ char *s, size_t n) 
  /*@modifies *pwc@*/ ;
int wctomb (/*@out@*/ /*@null@*/ char *s, wchar_t wchar) 
  /*@modifies *s@*/ ;
size_t mbstowcs (/*@out@*/ wchar_t *pwcs, char *s, size_t n)
  /*@modifies *pwcs@*/ ;
size_t wcstombs (/*@out@*/ char *s, wchar_t *pwcs, size_t n)
  /*@modifies *s@*/ ;


# ifdef UNIX

long a64l(const char *s) ;
char *l64a(long value)
  /*@ensures maxRead(result) <= 5 /\ maxSet(result) <= 5 @*/ ;

char *ecvt(double value, int ndigit, /*@out@*/ int *decpt, /*@out@*/ int *sign);

char *fcvt(double value, int ndigit, /*@out@*/ int *decpt,  /*@out@*/int *sign);

char *gcvt(double value, int ndigit, char *buf)
  /*@requires maxSet(buf) >= ndigit @*/ ;

/*@observer@*/ /*@null@*/
char *getenv (const char *name) /*@*/ ;

int getsubopt(char **optionp, char * const *tokens, /*@out@*/ char **valuep)
  /*@modifies optionp, valuep @*/ ;

int grantpt(int fildes)
  /*@modifies fileSystem, errno @*/ ;

/* specifying the array size is meaningless but we include
   it to be consistent with the unix specification at opengroup.org */
/*@-fixedformalarray@*/
unsigned short int *seed48 (unsigned short int seed16v[3])
  /*@modifies internalState@*/
  /*@requires maxRead(seed16v) >= 2 @*/ ; 

long int jrand48 (unsigned short int xsubi[3])
  /*@modifies internalState@*/
  /*@requires maxSet(xsubi) >= 2 @*/ ; 
     
long int nrand48 (unsigned short int xsubi[3]) /*@modifies internalState, xsubi @*/
  /*@requires maxSet(xsubi) >= 2 /\ maxRead(xsubi) >= 2 @*/ ;

void lcong48 (unsigned short int param[7])
  /*@modifies internalState@*/ /*@requires maxRead(param) >= 6 @*/ ; 
/*@=fixedformalarray@*/

long int lrand48 (void)
  /*@modifies internalState@*/ ; 

long int mrand48 (void)
  /*@modifies internalState@*/ ;

double drand48 (void)
  /*@modifies internalState@*/ ; 

void srand48 (long int seedval)
  /*@modifies internalState@*/ ;

int mblen (char *s, size_t n)
  /*@modifies errno@*/
  /*@requires maxRead(s) >= (n - 1) @*/ ;

size_t mbstowcs(/*@null@*/ /*@out@*/ wchar_t *pwcs, const char *s, size_t n)
  /*@requires maxSet(pwcs) >= (n - 1) @*/ ;

int mbtowc (/*@null@*/ /*@out@*/ wchar_t *pwc, /*@null@*/ char *s, size_t n) 
  /*@modifies *pwc, errno@*/  
  /*@requires maxRead(s) >= (n - 1) @*/ ;

char *mktemp(char *template)
  /*@warn legacy "mktemp is obsolete.  Use mkstemp instead." @*/
  /*@modifies template @*/ ;

int mkstemp(char *template)
  /*@modifies template, fileSystem @*/ ;

/*@dependent@*/
char *ptsname(int fildes) ;

int putenv (/*@kept@*/ const char *string)
  /*@globals environ@*/
  /*@modifies *environ, errno@*/ ;

char *initstate(unsigned int seed, char *state, size_t size)
  /*@modifies internalState, state @*/
  /*@requires maxSet(state) >= (size - 1) @*/ ;

/*@only@*/
char *setstate(/*@kept@*/ const char *state)
  /*@modifies internalState, errno@*/ ;
     
long random(void)
  /*@modifies internalState@*/ ;

void srandom(unsigned int seed)
  /*@modifies internalState@*/ ;

int rand_r(unsigned int *seed)
  /*@modifies seed@*/ ;
  
char *realpath(const char *file_name, /*@out@*/ char *resolved_name)
  /* annotation suppressed: @requires maxSet(resolved_name) >=  (PATH_MAX - 1) @*/ ;

void setkey(const char *key)
  /*@requires maxRead(key) >= 63 @*/
  /*@modifies internalState, errno@*/ ;

int ttyslot(void)
  /*@warn legacy "ttyslot is obsolete." @*/
  /*@*/ ;
  
int unlockpt(int fildes)
  /*@modifies fileSystem, internalState @*/ ;
     
void *valloc(size_t size)
  /*@warn legacy "valloc is obsolete.  Use posix_memalign instead." @*/
  /*@modifies errno@*/
  /*@ensures MaxSet(result) == (size - 1) @*/ ;

size_t wcstombs (/*@out@*/ char *s, wchar_t *pwcs, size_t n)
  /*@modifies *s, errno@*/
  /*@requires maxSet(s) >= (n - 1) @*/ ;

int wctomb (/*@out@*/ /*@null@*/ char *s, wchar_t wchar) 
  /*@modifies *s@*/ ;

double erand48 (unsigned short int /*@-fixedformalarray@*/ xsubi[3] /*@=fixedformalarray@*/ ) 
   /*@modifies internalState@*/ ; 

# endif /* UNIX */


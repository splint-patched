/*
** assert.h
*/

/*@constant _Bool NDEBUG;@*/

/*@falseexit@*/ void assert (/*@sef@*/ _Bool
# ifndef STRICT
        /*@alt int@*/
# endif
        e) /*@*/ ;


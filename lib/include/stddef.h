/*
** stddef.h
*/

typedef /*@integraltype@*/ ptrdiff_t;    
typedef /*@unsignedintegraltype@*/ size_t;
typedef /*@signedintegraltype@*/ ssize_t;

/*@constant null anytype NULL = 0;@*/

typedef /*@integraltype@*/ wchar_t;

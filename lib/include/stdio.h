/*
** stdio.h
*/

typedef /*@abstract@*/ /*@mutable@*/ void *FILE;
typedef /*@abstract@*/ /*@mutable@*/ void *fpos_t;

# ifdef UNIX
/*@constant _Bool _IOFBF; @*/
/*@constant _Bool _IOLBF; @*/
/*@constant _Bool _IONBF; @*/
# else
/*@constant size_t _IOFBF; @*/
/*@constant size_t _IOLBF; @*/
/*@constant size_t _IONBF; @*/
# endif

/*@constant size_t BUFSIZ; @*/

/*@constant int EOF; @*/

/*@constant unsignedintegraltype L_tmpnam; @*/
/*@constant observer char *P_tmpdir; @*/

/*@constant int SEEK_CUR; @*/
/*@constant int SEEK_END; @*/
/*@constant int SEEK_SET; @*/

/*@constant unsignedintegraltype FOPEN_MAX; @*/
/*@constant unsignedintegraltype FILENAME_MAX; @*/
/*@constant unsignedintegraltype TMP_MAX@; */


# ifdef STRICT
/*@checked@*/ FILE *stderr;
/*@checked@*/ FILE *stdin;
/*@checked@*/ FILE *stdout;
# else
/*@unchecked@*/ FILE *stderr;
/*@unchecked@*/ FILE *stdin;
/*@unchecked@*/ FILE *stdout;
# endif

int remove (char *filename)
  /*@modifies fileSystem, errno@*/ ;

int rename (char *old, char *new)
  /*@modifies fileSystem, errno@*/ ;

/*@dependent@*/ /*@null@*/
FILE *tmpfile (void)
  /*@modifies fileSystem, errno@*/ ;

/*@observer@*/
char * tmpnam (/*@out@*/ /*@null@*/ /*@returned@*/ char *s) 
  /*@warn toctou|legacy
          "Between the time a pathname is created and the file is opened, "
          "another process may create a file with the same name. POSIX.1-2008"
          "marks this as obsolete.  Use tmpfile instead." @*/
  /*@modifies *s, internalState@*/
  /* annotation suppressed: @requires maxSet(s) >= (L_tmpnam - 1) @*/ ;

/*@null@*/
char *tempnam (char *dir, /*@null@*/ char *pfx) 
  /*@warn toctou|legacy
          "Between the time a pathname is created and the file is opened, "
          "it is possible for some other process to create a file with "
          "the same name.  POSIX.1-2008 marks this as obsolete.  Use "
          "tmpfile instead." @*/
  /*@modifies internalState, errno@*/
  /*@ensures maxSet(result) >= 0 /\ maxRead(result) >= 0 @*/ ;

int fclose (FILE *stream) 
  /*@modifies *stream, errno, fileSystem@*/ ;

int fflush (/*@null@*/ FILE *stream) 
  /*@modifies *stream, errno, fileSystem;@*/ ;

/*@null@*/ /*@dependent@*/
FILE *fopen (char *filename, char *mode) 
   /*@modifies fileSystem@*/ ;         

/*@dependent@*/ /*@null@*/
FILE *freopen (char *filename, char *mode, FILE *stream) 
  /*@modifies *stream, fileSystem, errno@*/ ;

void setbuf (FILE *stream, /*@null@*/ /*@exposed@*/ /*@out@*/ char *buf) 
  /*@modifies fileSystem, *stream, *buf@*/ 
  /*:errorcode != 0*/ ;
  /*:requires maxSet(buf) >= (BUFSIZ - 1):*/ ;

int setvbuf (FILE *stream, /*@null@*/ /*@exposed@*/ /*@out@*/ char *buf, 
	     int mode, size_t size)
  /*@modifies fileSystem, *stream, *buf@*/
  /*@requires maxSet(buf) >= (size - 1) @*/ ;

/*@printflike@*/ 
int
# ifndef STRICT
/*@alt void@*/
# endif
fprintf (FILE *stream, char *format, ...)
  /*@modifies fileSystem, *stream@*/ ;

/*@scanflike@*/ 
int fscanf (FILE *stream, char *format, ...)
  /*@modifies fileSystem, *stream, errno@*/ ;

/*@printflike@*/ 
int
# ifndef STRICT
/*@alt void@*/
# endif
printf (char *format, ...) 
   /*@globals stdout@*/
   /*@modifies fileSystem, *stdout@*/ ;

/*@scanflike@*/
int scanf(char *format, ...)
   /*@globals stdin@*/
   /*@modifies fileSystem, *stdin, errno@*/ ;

/*@printflike@*/ 
int
# ifndef STRICT
/*@alt void@*/
# endif
sprintf (/*@out@*/ char *s, char *format, ...) 
  /*@warn bufferoverflowhigh
          "Buffer overflow possible with sprintf.  Recommend using snprintf instead" @*/
  /*@modifies *s@*/ ;

/*@printflike@*/
int snprintf (/*@out@*/ char * restrict s, size_t n, const char * restrict format, ...)
  /*@modifies s@*/
  /*@requires maxSet(s) >= (n - 1)@*/ ;

/*@scanflike@*/ 
int sscanf (/*@out@*/ char *s, char *format, ...) /*@modifies errno@*/ ;
/* modifies extra arguments */

int vprintf (const char *format, va_list arg)
  /*@globals stdout@*/
  /*@modifies fileSystem, *stdout@*/ ;

int vfprintf (FILE *stream, char *format, va_list arg)
  /*@modifies fileSystem, *stream, arg, errno@*/ ;

int vsprintf (/*@out@*/ char *str, const char *format, va_list ap)
  /*@warn bufferoverflowhigh "Use vsnprintf instead" @*/
  /*@modifies str@*/ ;

int vsnprintf (/*@out@*/ char *str, size_t size, const char *format, va_list ap)
  /*@requires maxSet(str) >= (size - 1)@*/ /* was size */
  /*@modifies str@*/ ;

int fgetc (FILE *stream) 
  /*@modifies fileSystem, *stream, errno@*/ ;

/*@null@*/
char * fgets (/*@returned@*/ /*@out@*/ char *s, int n, FILE *stream)
  /*@modifies fileSystem, *s, *stream, errno@*/
  /*@requires maxSet(s) >= (n -1); @*/
  /*@ensures maxRead(s) <= (n -1) /\ maxRead(s) >= 0; @*/ ;

int fputc (int /*@alt char@*/ c, FILE *stream)
  /*:errorcode EOF:*/
  /*@modifies fileSystem, *stream, errno@*/ ;

int fputs (char *s, FILE *stream)
  /*@modifies fileSystem, *stream@*/ ;

/* note use of sef --- stream may be evaluated more than once */
int getc (/*@sef@*/ FILE *stream)
  /*@modifies fileSystem, *stream, errno@*/ ;

int getchar (void)
  /*@globals stdin@*/ /*@modifies fileSystem, *stdin, errno@*/ ;

/*@null@*/
char *gets (/*@out@*/ char *s) 
  /*@warn bufferoverflowhigh
          "Use of gets leads to a buffer overflow vulnerability.  Use fgets instead" @*/
  /*@globals stdin@*/
  /*@modifies fileSystem, *s, *stdin, errno@*/ ;

int putc (int /*@alt char@*/ c, /*@sef@*/ FILE *stream)
   /*:errorcode EOF:*/
   /*@modifies fileSystem, *stream, errno;@*/ ;

int putchar (int /*@alt char@*/ c)
   /*:errorcode EOF:*/
   /*@globals stdout@*/ 
   /*@modifies fileSystem, *stdout, errno@*/ ; 

int puts (const char *s)
   /*:errorcode EOF:*/
   /*@globals stdout@*/
   /*@modifies fileSystem, *stdout, errno@*/ ; 

int ungetc (int /*@alt char@*/ c, FILE *stream)
  /*@modifies fileSystem, *stream@*/ ;

size_t 
  fread (/*@out@*/ void *ptr, size_t size, size_t nobj, FILE *stream)
  /*@modifies fileSystem, *ptr, *stream, errno@*/ 
  /*requires maxSet(ptr) >= (size - 1) @*/
  /*@ensures maxRead(ptr) == (size - 1) @*/ ;

size_t fwrite (void *ptr, size_t size, size_t nobj, FILE *stream)
  /*@modifies fileSystem, *stream, errno@*/ 
  /*@requires maxRead(ptr) >= size @*/ ;

int fgetpos (FILE *stream, /*@out@*/ fpos_t *pos)
  /*@modifies *pos, errno@*/
  /*@requires maxSet(pos) >= 0@*/
  /*@ensures maxRead(pos) >= 0 @*/;

int fseek (FILE *stream, long int offset, int whence)
  /*:errorcode -1:*/
  /*@modifies fileSystem, *stream, errno@*/ ;

int fsetpos (FILE *stream, fpos_t *pos)
  /*@modifies fileSystem, *stream, errno@*/ ;

long int ftell(FILE *stream) 
  /*:errorcode -1:*/ /*@modifies errno*/ ;

void rewind (FILE *stream)
  /*@modifies fileSystem, *stream@*/ ;

void clearerr (FILE *stream) /*@modifies *stream@*/ ;

int feof (FILE *stream) /*@modifies errno@*/ ;

int ferror (FILE *stream) /*@modifies errno@*/ ;

void perror (/*@null@*/ char *s) 
  /*@globals errno, stderr@*/
  /*@modifies fileSystem, *stderr@*/ ; 


# ifdef POSIX
/*@constant unsignedintegraltype L_ctermid@*/
/*@constant unsignedintegraltype L_cuserid@*/
/*@constant int STREAM_MAX@*/

/*@null@*/ /*@dependent@*/
FILE *fdopen (int fd, const char *type)
  /*@modifies errno, fileSystem@*/;

int fileno (/*@notnull@*/ FILE *)
  /*:errorcode -1:*/ 
  /*@modifies errno@*/ ;

int fseeko (FILE *stream, off_t offset, int whence)
  /*:errorcode -1:*/
  /*@modifies stream, errno@*/ ;

off_t ftello(FILE *stream)
  /*:errorcode -1:*/
  /*@modifies errno*/ ;

int getc_unlocked(FILE *stream)
  /*@warn multithreaded "getc_unlocked is a thread unsafe version of getc" @*/
  /*@modifies *stream, fileSystem, errno@*/ ;

int getchar_unlocked (void)
  /*@warn multithreaded "getchar_unlocked is a thread unsafe version of getchar" @*/
  /*@globals stdin@*/
  /*@modifies *stdin, fileSystem@*/ ;

int putc_unlocked (int, FILE *stream)
  /*@warn multithreaded "putc_unlocked is a thread unsafe version of putc" @*/
  /*:errorcode EOF:*/
  /*@modifies fileSystem, *stream, errno@*/ ;

int putchar_unlocked(int)
  /*@warn multithreaded "putchar_unlocked is a thread unsafe version of putchar" @*/
  /*:errorcode EOF:*/
  /*@modifies fileSystem, *stdout, errno@*/ ;

/* Result may be static pointer if parameter is NULL, otherwise is fresh. */
/*@dependent@*/ /*@null@*/ 
char * ctermid (/*@returned@*/ /*@out@*/ /*@null@*/ char *s)
  /*@modifies *s, systemState@*/;

/*@dependent@*/ /*@null@*/
char *cuserid (/*@returned@*/ /*@null@*/ /*@out@*/ char *s)
  /*@warn legacy "cuserid is obsolete." @*/ 
  /* cuserid is in the 1988 version of POSIX but removed in 1990 */
  /*@modifies *s@*/
  /* annotation suppressed: @requires maxSet(s) >= ( L_cuserid - 1) @*/
  /* annotation suppressed: @ensures maxRead(s) <= ( L_cuserid - 1) /\ maxRead(s) >= 0  /\
                                     maxRead(result) <= ( L_cuserid - 1) /\ maxRead(result) >= 0 @*/
  ;

# endif /* POSIX */

# ifdef UNIX
void flockfile (/*@notnull@*/ FILE *f)
  /*@modifies f, fileSystem@*/ ;

int ftrylockfile(FILE *stream)
  /*:errorcode !0:*/
  /*@modifies stream, fileSystem, errno*/ ;

void funlockfile (FILE *stream)
  /*@modifies stream, fileSystem*/ ;

int getw (FILE *stream)
  /*@warn legacy@*/ 
  /*:errorcode EOF:*/
  /*@modifies fileSystem, *stream, errno@*/ ;

int pclose(FILE *stream)
  /*:errorcode -1:*/
  /*@modifies fileSystem, *stream, errno@*/ ;

/*@dependent@*/ /*@null@*/
FILE *popen (const char *command, const char *mode)
  /*:errorcode NULL:*/
  /*@modifies fileSystem, errno@*/ ;

int putw(int, FILE *stream)
  /*@warn legacy@*/ 
  /*:errorcode EOF:*/
  /*@modifies fileSystem, *stdout, errno@*/ ;

int setvbuf_unlocked (FILE *stream, /*@null@*/ /*@exposed@*/ char *buf, 
    int mode, size_t size)
  /*@modifies internalState@*/ ;

void setbuffer (FILE *stream, /*@null@*/ /*@exposed@*/ char *buf, int size)
  /*@modifies internalState@*/ ;

void setlinebuf (FILE *stream) /*@modifies internalState@*/ ;

size_t fread_unlocked (/*@out@*/ void *ptr, size_t size, size_t nitems, 
    FILE *stream) 
  /*@modifies *stream, *ptr@*/ ;

size_t fwrite_unlocked (void *pointer, size_t size, size_t num_items, FILE *stream)
  /*@modifies *stream@*/ ;

# endif /* UNIX */


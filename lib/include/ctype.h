/*
** ctype.h
*/

# ifdef STRICT
/*@notfunction@*/
#  define SBOOLINT _Bool
/*@notfunction@*/
#  define SINTCHAR int
# else
/*@notfunction@*/
#  define SBOOLINT _Bool /*@alt int@*/
/*@notfunction@*/
#  define SINTCHAR int /*@alt signed char, unsigned char@*/
# endif

SBOOLINT isalnum (SINTCHAR) /*@*/ ;
SBOOLINT isalpha (SINTCHAR) /*@*/ ;
SBOOLINT iscntrl (SINTCHAR) /*@*/ ;
SBOOLINT isdigit (SINTCHAR) /*@*/ ;
SBOOLINT isgraph (SINTCHAR) /*@*/ ;
SBOOLINT islower (SINTCHAR) /*@*/ ;
SBOOLINT isprint (SINTCHAR) /*@*/ ;
SBOOLINT ispunct (SINTCHAR) /*@*/ ;
SBOOLINT isspace (SINTCHAR) /*@*/ ;
SBOOLINT isupper (SINTCHAR) /*@*/ ;
SBOOLINT isxdigit (SINTCHAR) /*@*/ ;
SINTCHAR tolower (SINTCHAR) /*@*/ ;
SINTCHAR toupper (SINTCHAR) /*@*/ ;

# ifdef UNIX
SBOOLINT isascii(SINTCHAR) /*@*/ ;
SINTCHAR toascii(SINTCHAR) /*@*/;
SINTCHAR _toupper(/*@sef@*/ SINTCHAR) /*@*/;
SINTCHAR _tolower(/*@sef@*/ SINTCHAR) /*@*/;
# endif /* UNIX */


/*
** dirent.h
*/

typedef /*@abstract@*/ /*@mutable@*/ void *DIR;

struct dirent {
  ino_t  d_ino;
  char	d_name[];
};

int closedir (DIR *dirp)
  /*@modifies errno@*/;

/*@null@*/ /*@dependent@*/
DIR *opendir (const char *dirname)
  /*@modifies errno, fileSystem@*/;

/*@dependent@*/ /*@null@*/
struct dirent *readdir (DIR *dirp)
  /*@modifies errno@*/;

void rewinddir (DIR *dirp) /*@*/;

# ifdef UNIX
int readdir_r (DIR *, struct dirent *, /*@out@*/ struct dirent **result)
     /*@modifies *result@*/
     /*:errorcode !0:*/ ;

void seekdir(DIR *, long int);
long int telldir(DIR *);
# endif

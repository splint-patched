/*
** signal.h
*/

/*@constant int SIGABRT; @*/
/*@constant int SIGFPE; @*/
/*@constant int SIGILL; @*/
/*@constant int SIGINT; @*/
/*@constant int SIGSEGV; @*/
/*@constant int SIGTERM; @*/

typedef /*@integraltype@*/ sig_atomic_t;

/*@constant void (*SIG_DFL)(int); @*/
/*@constant void (*SIG_ERR)(int); @*/
/*@constant void (*SIG_IGN)(int); @*/

typedef void (*sighandler_t) (int);

/*
** signal takes an int and a sighandler_t, and returns a sighandler_t
** (possibly NULL).
*/

/*@null@*/
sighandler_t signal (int sig, /*@null@*/ sighandler_t func)
  /*@modifies internalState, errno;@*/ ;

/*@mayexit@*/ int raise (int sig) ;


# ifdef POSIX
typedef /*@abstract@*/ /*@immutable@*/ sigset_t;

typedef struct {
  void *ss_sp;
  size_t ss_size;
  int ss_flags;
} stack_t;

/*@constant int SA_NOCLDSTOP@*/
/*@constant int SIG_BLOCK@*/
/*@constant int SIG_SETMASK@*/
/*@constant int SIG_UNBLOCK@*/
/*@constant int SIGALRM@*/
/*@constant int SIGCHLD@*/
/*@constant int SIGCONT@*/
/*@constant int SIGHUP@*/
/*@constant int SIGKILL@*/
/*@constant int SIGPIPE@*/
/*@constant int SIGQUIT@*/
/*@constant int SIGSTOP@*/
/*@constant int SIGTSTP@*/
/*@constant int SIGTTIN@*/
/*@constant int SIGTTOU@*/
/*@constant int SIGUSR1@*/
/*@constant int SIGUSR2@*/

struct sigstack {
  int ss_onstack;
  void *ss_sp;
} ;

typedef struct {
  int si_signo;
  int si_errno;
  int si_code;
  pid_t si_pid;
  uid_t si_uid;
  void *si_addr;
  int si_status;
  long si_band;
  union sigval si_value;
} siginfo_t;

typedef union {
  int    sival_int;
  void  *sival_ptr;    
} sigval;

struct sigaction {
  void (*sa_handler)();
  sigset_t sa_mask;
  int sa_flags;
  void (*sa_sigaction)(int, siginfo_t *, void *);
} ;
 
/*@mayexit@*/
int kill (pid_t pid, int sig)
  /*@modifies errno@*/;

int sigaction (int sig, /*@null@*/ const struct sigaction *act,
    /*@out@*/ /*@null@*/ struct sigaction *oact)
  /*@modifies *oact, errno, systemState@*/ ;

int sigaddset (sigset_t *set, int signo)
  /*@modifies *set, errno@*/ ;

int sigdelset (sigset_t *set, int signo)
  /*@modifies *set, errno@*/ ;

int sigemptyset (/*@out@*/ sigset_t *set)
  /*@modifies *set, errno@*/ ;

int sigfillset (/*@out@*/ sigset_t *set)
  /*@modifies *set, errno@*/ ;

int sigismember (const sigset_t *set, int signo)
  /*@modifies errno@*/ ;

int sigpending (/*@out@*/ sigset_t *set)
  /*@modifies *set, errno@*/ ;

int sigprocmask (int how, /*@null@*/ const sigset_t *set,
    /*@null@*/ /*@out@*/ sigset_t *oset)
  /*@modifies *oset, errno, systemState@*/ ;

int sigsuspend (const sigset_t *sigmask)
  /*@modifies errno, systemState@*/ ;

# endif /* POSIX */

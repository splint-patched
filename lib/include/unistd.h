/*
** unistd.h
*/

/* for access: */
/*@constant int F_OK@*/
/*@constant int R_OK@*/
/*@constant int W_OK@*/
/*@constant int X_OK@*/

/*@constant int STDERR_FILENO@*/
/*@constant int STDIN_FILENO@*/
/*@constant int STDOUT_FILENO@*/
/*@constant int _PC_CHOWN_RESTRUCTED@*/
/*@constant int _PC_MAX_CANON@*/
/*@constant int _PC_MAX_INPUT@*/
/*@constant int _PC_NAME_MAX@*/
/*@constant int _PC_NO_TRUNC@*/
/*@constant int _PC_PATH_MAX@*/
/*@constant int _PC_PIPE_BUF@*/
/*@constant int _PC_VDISABLE@*/
/*@constant int _POSIX_CHOWN_RESTRICTED@*/
/*@constant int _POSIX_JOB_CONTROL@*/
/*@constant int _POSIX_NO_TRUNC@*/
/*@constant int _POSIX_SAVED_IDS@*/
/*@constant int _POSIX_VDISABLE@*/
/*@constant int _POSIX_VERSION@*/
/*@constant int _SC_ARG_MAX@*/
/*@constant int _SC_CHILD_MAX@*/
/*@constant int _SC_CLK_TCK@*/
/*@constant int _SC_JOB_CONTROL@*/
/*@constant int _SC_NGROUPS_MAX@*/
/*@constant int _SC_OPEN_MAX@*/
/*@constant int _SC_SAVED_IDS@*/
/*@constant int _SC_STREAM_MAX@*/
/*@constant int _SC_TZNAME_MAX@*/
/*@constant int _SC_VERSION@*/

/* for confstr(): */
/*@constant int _CS_POSIX_V7_ILP32_OFF32_CFLAGS @*/
/*@constant int _CS_POSIX_V7_ILP32_OFF32_LDFLAGS @*/
/*@constant int _CS_POSIX_V7_ILP32_OFF32_LIBS @*/
/*@constant int _CS_POSIX_V7_ILP32_OFFBIG_CFLAGS @*/
/*@constant int _CS_POSIX_V7_ILP32_OFFBIG_LDFLAGS @*/
/*@constant int _CS_POSIX_V7_ILP32_OFFBIG_LIBS @*/
/*@constant int _CS_POSIX_V7_LP64_OFF64_CFLAGS @*/
/*@constant int _CS_POSIX_V7_LP64_OFF64_LDFLAGS @*/
/*@constant int _CS_POSIX_V7_LP64_OFF64_LIBS @*/
/*@constant int _CS_POSIX_V7_LPBIG_OFFBIG_CFLAGS @*/
/*@constant int _CS_POSIX_V7_LPBIG_OFFBIG_LDFLAGS @*/
/*@constant int _CS_POSIX_V7_LPBIG_OFFBIG_LIBS @*/
/*@constant int _CS_POSIX_V7_THREADS_CFLAGS @*/
/*@constant int _CS_POSIX_V7_THREADS_LDFLAGS @*/
/*@constant int _CS_POSIX_V7_WIDTH_RESTRICTED_ENVS @*/
/*@constant int _CS_V7_ENV @*/
/* [obsolete constants] for confstr(): */
/*@constant int _CS_POSIX_V6_ILP32_OFF32_CFLAGS @*/
/*@constant int _CS_POSIX_V6_ILP32_OFF32_LDFLAGS @*/
/*@constant int _CS_POSIX_V6_ILP32_OFF32_LIBS @*/
/*@constant int _CS_POSIX_V6_ILP32_OFFBIG_CFLAGS @*/
/*@constant int _CS_POSIX_V6_ILP32_OFFBIG_LDFLAGS @*/
/*@constant int _CS_POSIX_V6_ILP32_OFFBIG_LIBS @*/
/*@constant int _CS_POSIX_V6_LP64_OFF64_CFLAGS @*/
/*@constant int _CS_POSIX_V6_LP64_OFF64_LDFLAGS @*/
/*@constant int _CS_POSIX_V6_LP64_OFF64_LIBS @*/
/*@constant int _CS_POSIX_V6_LPBIG_OFFBIG_CFLAGS @*/
/*@constant int _CS_POSIX_V6_LPBIG_OFFBIG_LDFLAGS @*/
/*@constant int _CS_POSIX_V6_LPBIG_OFFBIG_LIBS @*/
/*@constant int _CS_POSIX_V6_WIDTH_RESTRICTED_ENVS @*/
/*@constant int _CS_V6_ENV @*/


/*@unchecked@*/ extern char **environ;
/*@unchecked@*/ extern char *optarg;
/*@unchecked@*/ extern int optind;
/*@unchecked@*/ extern int optopt;
/*@unchecked@*/ extern int opterr;

/*@exits@*/
void _exit (int status) /*@*/;

int access (const char *path, int mode)
  /*@modifies errno@*/;

unsigned int alarm (unsigned int)
  /*@modifies systemState@*/;

int chdir (const char *path)
  /*@modifies errno, internalState@*/;

int fchdir (int)
  /*@modifies errno, fileSystem@*/
  /*:errorcode -1:*/ ;

int chown (const char *path, uid_t owner, gid_t group)
  /*@modifies fileSystem, errno@*/;

int fchown (int, uid_t, gid_t)
  /*@modifies errno, fileSystem@*/
  /*:errorcode -1:*/ ;

int lchown(const char *, uid_t, gid_t)
  /*@modifies errno, fileSystem@*/
  /*:errorcode -1:*/ ;

int close (int fd)
  /*@modifies fileSystem, errno, systemState@*/;
  /* state: record locks are unlocked */

int dup2 (int fd, int fd2)
  /*@modifies errno, fileSystem@*/;

int dup (int fd)
  /*@modifies errno, fileSystem@*/;

/*@mayexit@*/
int execl (const char *path, const char *arg, ...)
  /*@modifies errno, internalState@*/;

/*@mayexit@*/
int execle (const char *file, const char *arg, ...)
  /*@modifies errno, internalState@*/;

/*@mayexit@*/
int execlp (const char *file, const char *arg, ...)
  /*@modifies errno, internalState@*/;

/*@mayexit@*/
int execv (const char *path, char *const argv[])
  /*@modifies errno, internalState@*/;

/*@mayexit@*/
int execve (const char *path, char *const argv[], char *const *envp)
  /*@modifies errno, internalState@*/;

/*@mayexit@*/
int execvp (const char *file, char *const argv[])
  /*@modifies errno, internalState@*/;

pid_t fork (void)
  /*@modifies errno, fileSystem@*/;

long fpathconf (int fd, int name)
  /*@modifies errno, internalState@*/;

/*@null@*/
char *getcwd (/*@returned@*/ /*@out@*/ /*@notnull@*/ char *buf, size_t size)
  /*@requires maxSet(buf) >= (size - 1)@*/
  /*@ensures  maxRead(buf) <= (size - 1)@*/
  /*@modifies errno, *buf@*/ ;

/*@null@*/
char *getwd (/*@returned@*/ char *path_name)
  /*@warn legacy "getwd is obsolete.  Use getcwd instead" @*/
  /*@modifies path_name@*/ ;


gid_t getegid (void) /*@*/;
uid_t geteuid (void) /*@*/;
gid_t getgid (void) /*@*/;

int getgroups (int gidsetsize, /*@out@*/ gid_t grouplist[])
  /*@requires maxSet(grouplist) >= gidsetsize@*/
  /*@modifies errno, grouplist[]@*/;

/*@null@*/ /*@observer@*/
char * getlogin (void) /*@modifies errno@*/ ;

int getlogin_r (char *name, size_t namesize)
  /*@requires maxSet(name) >= namesize@*/
  /*:errorcode !0:*/ ;

pid_t getpgrp (void) /*@*/;
pid_t getpid (void) /*@*/;
pid_t getppid (void) /*@*/;
uid_t getuid (void) /*@*/;

int isatty (int fd) /*@modifies errno@*/;

int link (const char *o, const char *n)
  /*@modifies errno, fileSystem@*/;

off_t lseek (int fd, off_t offset, int whence) /*@modifies errno, fileSystem@*/;

long pathconf (const char *path, int name)
  /*@modifies errno@*/;

int pause (void)
  /*@modifies errno@*/;

/* Out parameter noticed by Marc Espie. */
int pipe (/*@out@*/ int fd[])
  /*@requires maxRead(fd) == 1@*/
  /*@modifies errno@*/;

ssize_t read (int fd, /*@out@*/ void *buf, size_t nbyte)
  /*@modifies errno, *buf@*/
  /*@requires maxSet(buf) >= (nbyte - 1) @*/
  /*@ensures maxRead(buf) >= nbyte @*/ ;

int rmdir (const char *path)
  /*@modifies fileSystem, errno@*/;

int setgid (gid_t gid)
  /*@modifies errno, systemState@*/;

int setpgid (pid_t pid, pid_t pgid)
  /*@modifies errno, systemState@*/;

pid_t setsid (void) /*@modifies errno, systemState@*/;

int setuid (uid_t uid)
  /*@modifies errno, systemState@*/;

unsigned int sleep (unsigned int sec) /*@modifies systemState@*/ ;

long sysconf (int name)
  /*@modifies errno@*/;

pid_t tcgetpgrp (int fd)
  /*@globals systemState@*/
  /*@modifies errno@*/;

int tcsetpgrp (int fd, pid_t pgrpid)
  /*@modifies errno, systemState@*/;

/*@null@*/ /*@observer@*/ /* Question: observer ok? */
char *ttyname (int fd)
  /*@globals systemState@*/
  /*@modifies errno@*/;

int ttyname_r(int, char *name, size_t namesize)
  /*@requires maxSet(name) >= (namesize - 1)@*/ ;
  /*:errorcode !0:*/ ;

int unlink (const char *path)
  /*@modifies fileSystem, errno@*/;

ssize_t write (int fd, const void *buf, size_t nbyte)
  /*@requires maxRead(buf) >= nbyte@*/
  /*@modifies errno@*/;

void sync (void)
  /*@modifies fileSystem@*/;
     
int fsync(int)
  /*@modifies errno, fileSystem@*/
  /*:errorcode -1:*/ ;

int fdatasync (int)
  /*@modifies errno, fileSystem@*/
  /*:errorcode -1:*/ ;

int symlink (char *name1, char *name2)
  /*@modifies errno, fileSystem@*/
  /*:errorcode -1:*/ ;

useconds_t ualarm(useconds_t, useconds_t)
  /*@warn legacy "ualarm is obsolete" @*/
  /*@modifies systemState@*/ ;

int usleep (useconds_t useconds)
  /*@warn legacy "usleep is obsolete.  Use nanosleep instead" @*/
  /*@modifies systemState, errno@*/
  /*error -1 sucess 0 */ ;

int truncate(const char *name, off_t length)
  /*@modifies errno, fileSystem@*/
  /*:errorcode -1:*/ ;
     
int ftruncate(int fd, off_t length)
  /*@modifies errno, fileSystem@*/
  /*:errorcode -1:*/ ;

int gethostname (/*@out@*/ char *address, size_t address_len) 
  /*:errorstatus@*/
  /*@modifies address@*/ ;

/*@dependent@*/ /*@null@*/
char *getpass(/*@nullterminated@*/ const char *)
  /*@warn legacy "getpass is obsolete"@*/ ;

int brk(void *)
  /*@modifies errno@*/
  /*:errorcode -1:*/
  /*@warn legacy "brk is obsolete"@*/ ;

void *sbrk(intptr_t)
  /*@modifies errno@*/
  /*:errorcode (void *)-1:*/
  /*@warn legacy "sbrk is obsolete"@*/ ;

size_t confstr(int, /*@null@*/ char *, size_t)
  /*@globals internalState@*/
  /*@modifies errno@*/
  /*:errorcode 0:*/ ;

/*@dependent@*/ /*@null@*/
char *crypt(const char *, const char *)
  /*@modifies errno, internalState@*/ ;

void encrypt(char p_block[], int)
  /*@requires maxSet(p_block) == 63@*/
  /*@modifies p_block, errno@*/ ;

int lockf(int, int, off_t)
  /*@modifies errno, fileSystem@*/
  /*:errorcode -1:*/ ;

int nice(int)
  /*@modifies errno, fileSystem@*/
  /*:errorcode -1:*/ ;

int getopt (int argc, char * const argv[], const char *optstring)
  /*@globals optarg, optind, optopt, opterr@*/
  /*@modifies optarg, optind, optopt@*/
  /*@requires maxRead(argv) >= (argc - 1) @*/
  /*:errorcode -1:*/ ;

int readlink(const char *, char *buf, size_t bufsize)
  /*@requires maxSet(buf) >= (bufsize - 1)@*/
  /*@modifies errno, fileSystem, *buf@*/
  /*:errorcode -1:*/ ;

int getpagesize(void)
  /*@warn legacy "getpagesize is obsolete"@*/ ;

long gethostid (void) /*@globals internalState@*/ ;

pid_t vfork(void)
  /*@warn legacy "vfork is obsolete" @*/
  /*@modifies errno, fileSystem@*/
  /*:errorcode -1:*/ ;

pid_t getpgid(pid_t)
  /*@modifies errno@*/
  /*@globals internalState@*/
  /*:errorcode (pid_t)-1:*/ ;

pid_t getsid(pid_t)
  /*@modifies errno@*/
  /*@globals internalState@*/
  /*:errorcode (pid_t)-1:*/ ;

pid_t setpgrp(void) /*@modifies internalState@*/;

ssize_t pread(int, /*@out@*/ void *buf, size_t nbyte, off_t offset)
  /*@modifies errno, fileSystem@*/
  /*@requires maxSet(buf) >= (nbyte - 1) @*/
  /*@ensures maxRead(buf) >= nbyte @*/ 
  /*:errorcode -1:*/ ;
     
ssize_t pwrite(int, const void *buf, size_t nbyte, off_t)
  /*@requires maxRead(buf) >= nbyte@*/
  /*@modifies errno, fileSystem@*/
  /*:errorcode -1:*/ ;

void swab(/*@unique@*/ const void *src, /*@unique@*/ void *dest, ssize_t nbytes)
  /*@requires maxSet(dest) >= (nbytes - 1)@*/ ;


# ifdef UNIX
int chroot (/*@notnull@*/ /*@nullterminated@*/ const char *path)
  /*@modifies internalState, errno@*/
  /*:errorcode -1:*/ 
  /*@warn superuser "Only super-user processes may call chroot."@*/ ;

int fchroot (int fildes)
  /*:statusreturn@*/
  /*@warn superuser "Only super-user processes may call fchroot."@*/ ;

int getdtablesize (void)
  /*@warn legacy "getdtablesize is obsolete"@*/ ;

/*@constant int _POSIX2_VERSION@*/
/*@constant int _POSIX2_C_VERSION@*/
/*@constant int _XOPEN_VERSION@*/
/*@constant int _XOPEN_XCU_VERSION@*/

/* for confstr(): */
/*@constant int _CS_PATH@*/
/*@constant int _CS_XBS5_ILP32_OFF32_CFLAGS@*/
/*@constant int _CS_XBS5_ILP32_OFF32_LDFLAGS@*/
/*@constant int _CS_XBS5_ILP32_OFF32_LIBS@*/
/*@constant int _CS_XBS5_ILP32_OFF32_LINTFLAGS@*/
/*@constant int _CS_XBS5_ILP32_OFFBIG_CFLAGS@*/
/*@constant int _CS_XBS5_ILP32_OFFBIG_LDFLAGS@*/
/*@constant int _CS_XBS5_ILP32_OFFBIG_LIBS@*/
/*@constant int _CS_XBS5_ILP32_OFFBIG_LINTFLAGS@*/
/*@constant int _CS_XBS5_LP64_OFF64_CFLAGS@*/
/*@constant int _CS_XBS5_LP64_OFF64_LDFLAGS@*/
/*@constant int _CS_XBS5_LP64_OFF64_LIBS@*/
/*@constant int _CS_XBS5_LP64_OFF64_LINTFLAGS@*/
/*@constant int _CS_XBS5_LPBIG_OFFBIG_CFLAGS@*/
/*@constant int _CS_XBS5_LPBIG_OFFBIG_LDFLAGS@*/
/*@constant int _CS_XBS5_LPBIG_OFFBIG_LIBS@*/
/*@constant int _CS_XBS5_LPBIG_OFFBIG_LINTFLAGS@*/


/* name parameters to sysconf: */

/*@constant int _SC_2_C_BIND@*/
/*@constant int _SC_2_C_DEV@*/
/*@constant int _SC_2_C_VERSION@*/
/*@constant int _SC_2_FORT_DEV@*/
/*@constant int _SC_2_FORT_RUN@*/
/*@constant int _SC_2_LOCALEDEF@*/
/*@constant int _SC_2_SW_DEV@*/
/*@constant int _SC_2_UPE@*/
/*@constant int _SC_2_VERSION@*/
/*@constant int _SC_AIO_LISTIO_MAX@*/
/*@constant int _SC_AIO_MAX@*/
/*@constant int _SC_AIO_PRIO_DELTA_MAX@*/
/*@constant int _SC_ASYNCHRONOUS_IO@*/
/*@constant int _SC_ATEXIT_MAX@*/
/*@constant int _SC_BC_BASE_MAX@*/
/*@constant int _SC_BC_DIM_MAX@*/
/*@constant int _SC_BC_SCALE_MAX@*/
/*@constant int _SC_BC_STRING_MAX@*/
/*@constant int _SC_COLL_WEIGHTS_MAX@*/
/*@constant int _SC_DELAYTIMER_MAX@*/
/*@constant int _SC_EXPR_NEST_MAX@*/
/*@constant int _SC_FSYNC@*/
/*@constant int _SC_GETGR_R_SIZE_MAX@*/
/*@constant int _SC_GETPW_R_SIZE_MAX@*/
/*@constant int _SC_IOV_MAX@*/
/*@constant int _SC_LINE_MAX@*/
/*@constant int _SC_LOGIN_NAME_MAX@*/
/*@constant int _SC_MAPPED_FILES@*/
/*@constant int _SC_MEMLOCK@*/
/*@constant int _SC_MEMLOCK_RANGE@*/
/*@constant int _SC_MEMORY_PROTECTION@*/
/*@constant int _SC_MESSAGE_PASSING@*/
/*@constant int _SC_MQ_OPEN_MAX@*/
/*@constant int _SC_MQ_PRIO_MAX@*/
/*@constant int _SC_PAGESIZE@*/
/*@constant int _SC_PAGE_SIZE@*/
/*@constant int _SC_PASS_MAX@*/
/*@constant int _SC_PRIORITIZED_IO@*/
/*@constant int _SC_PRIORITY_SCHEDULING@*/
/*@constant int _SC_RE_DUP_MAX@*/
/*@constant int _SC_REALTIME_SIGNALS@*/
/*@constant int _SC_RTSIG_MAX@*/
/*@constant int _SC_SEMAPHORES@*/
/*@constant int _SC_SEM_NSEMS_MAX@*/
/*@constant int _SC_SEM_VALUE_MAX@*/
/*@constant int _SC_SHARED_MEMORY_OBJECTS@*/
/*@constant int _SC_SIGQUEUE_MAX@*/
/*@constant int _SC_SYNCHRONIZED_IO@*/
/*@constant int _SC_THREADS@*/
/*@constant int _SC_THREAD_ATTR_STACKADDR@*/
/*@constant int _SC_THREAD_ATTR_STACKSIZE@*/
/*@constant int _SC_THREAD_DESTRUCTOR_ITERATIONS@*/
/*@constant int _SC_THREAD_KEYS_MAX@*/
/*@constant int _SC_THREAD_PRIORITY_SCHEDULING@*/
/*@constant int _SC_THREAD_PRIO_INHERIT@*/
/*@constant int _SC_THREAD_PRIO_PROTECT@*/
/*@constant int _SC_THREAD_PROCESS_SHARED@*/
/*@constant int _SC_THREAD_SAFE_FUNCTIONS@*/
/*@constant int _SC_THREAD_STACK_MIN@*/
/*@constant int _SC_THREAD_THREADS_MAX@*/
/*@constant int _SC_TIMERS@*/
/*@constant int _SC_TIMER_MAX@*/
/*@constant int _SC_TTY_NAME_MAX@*/
/*@constant int _SC_XOPEN_VERSION@*/
/*@constant int _SC_XOPEN_CRYPT@*/
/*@constant int _SC_XOPEN_ENH_I18N@*/
/*@constant int _SC_XOPEN_SHM@*/
/*@constant int _SC_XOPEN_UNIX@*/
/*@constant int _SC_XOPEN_XCU_VERSION@*/
/*@constant int _SC_XOPEN_LEGACY@*/
/*@constant int _SC_XOPEN_REALTIME@*/
/*@constant int _SC_XOPEN_REALTIME_THREADS@*/
/*@constant int _SC_XBS5_ILP32_OFF32@*/
/*@constant int _SC_XBS5_ILP32_OFFBIG@*/
/*@constant int _SC_XBS5_LP64_OFF64@*/
/*@constant int _SC_XBS5_LPBIG_OFFBIG@*/

int pthread_atfork(void (*)(void), void (*)(void), void(*)(void))
  /*@modifies errno, fileSystem@*/
  /*:errorcode !0:*/ ;
# endif



/* Note: return value is NOT like strcmp. */
int /*@alt _Bool@*/ bcmp (const void *b1, const void * b2, size_t length)
  /*@warn legacy "bcmp is obsolete" @*/
  /*@*/ ;

/* Note: the second parameter is the out param. */
void bcopy (const void *b1, /*@out@*/ void *b2, size_t length) 
  /*@warn legacy "bcopy is obsolete" @*/
  /*@modifies *b2@*/ ;

void bzero (/*@out@*/ char *b1, int length)
  /*@warn legacy "bzero is obsolete" @*/
  /*@modifies *b1@*/ ;

int ffs (int i) /*@*/ ;

/*@null@*/ /*@dependent@*/
char * index (/*@returned@*/ char *s, char c)
  /*@warn legacy "index is obsolete" @*/
  /*@*/ ;

/*@null@*/ /*@dependent@*/
char * rindex (/*@returned@*/ char *s, char c)
  /*@warn legacy "rindex is obsolete" @*/
  /*@*/ ;

int strcasecmp (char *s1, char *s2) /*@*/ ;
int strncasecmp (char *s1, char *s2, int n) /*@*/ ;


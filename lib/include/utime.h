/*
** utime.h
*/

struct utimbuf {
  time_t	actime;
  time_t	modtime;
} ;

int utime (const char *path, /*@null@*/ const struct utimbuf *times)
  /*@modifies fileSystem, errno@*/;


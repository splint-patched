/*
** pwd.h
*/

struct passwd {
  char *pw_name;
  uid_t pw_uid;
  gid_t pw_gid;
  char *pw_dir;
  char *pw_shell;
} ;

/*@observer@*/ /*@null@*/
struct passwd *getpwnam (const char *)
  /*@modifies errno@*/
  /*@ensures maxRead(result) == 0 /\ maxSet(result) == 0 @*/ ;

/*@observer@*/ /*@null@*/
struct passwd *getpwuid (uid_t uid)
  /*@modifies errno@*/ 
  /*@ensures maxRead(result) == 0 /\ maxSet(result) == 0 @*/ ;


#ifndef SPLINT_BOOL_H
#define SPLINT_BOOL_H

#ifndef FALSE
#define FALSE 0
#endif

#ifndef TRUE
#define TRUE (!FALSE)
#endif

/* bool is a keyword in C++ */
/*@-cppnames@*/
typedef int bool;
/*@=cppnames@*/

# endif /* SPLINT_BOOL_H */

/*
** unix.h
*/

/*
** Extra stuff in some unixen, not in posix.
*/

/*@constant int LOCK_MAX@*/
/*@constant int FCHR_MAX@*/
/*@constant int USI_MAX@*/
/*@constant int WORD_BIT@*/
/*@constant int LONG_BIT@*/

/*@constant int MAX_CHAR@*/
/*@constant int PASS_MAX@*/
/*@constant int PID_MAX@*/
/*@constant int SYSPID_MAX@*/

/*@constant int PIPE_MAX@*/
/*@constant int PROC_MAX@*/
/*@constant int STD_BLK@*/
/*@constant int SYS_NMLN@*/
/*@constant int SYS_OPEN@*/
/*@constant int NZERO@*/
/*@constant int UID_MAX@*/
/*@constant int NL_ARGMAX@*/
/*@constant int NL_MSGMAX@*/
/*@constant int NL_NMAX@*/
/*@constant int NL_SETMAX@*/
/*@constant int NL_TEXTMAX@*/
/*@constant int NL_LBLMAX@*/
/*@constant int NL_LANGMAX @*/
 
/*@constant int DOMAIN@*/
/*@constant int SING@*/
/*@constant int OVERFLOW@*/
/*@constant int UNDERFLOW@*/
/*@constant int TLOSS@*/
/*@constant int PLOSS@*/

typedef	unsigned char uchar_t;
typedef	unsigned short ushort_t;
typedef	unsigned int uint_t;
typedef unsigned long ulong_t;
typedef	volatile unsigned char vuchar_t;
typedef	volatile unsigned short	vushort_t;
typedef	volatile unsigned int vuint_t;
typedef volatile unsigned long vulong_t;
typedef	/*@integraltype@*/ daddr_t;
typedef	char *caddr_t;	
typedef long *qaddr_t; 
typedef char *addr_t;
typedef long physadr_t;
typedef short cnt_t;
typedef	int chan_t;	
typedef	int paddr_t;
typedef	void *mid_t;
typedef char slab_t[12];	
typedef ulong_t	shmatt_t;	
typedef ulong_t	msgqnum_t;	
typedef ulong_t	msglen_t;
typedef	uchar_t uchar;
typedef	ushort_t ushort;
typedef	uint_t uint;
typedef ulong_t	ulong;
typedef	uchar_t	u_char;
typedef	ushort_t u_short;
typedef	uint_t u_int;
typedef	ulong_t	u_long;
typedef	vuchar_t vu_char;
typedef	vushort_t vu_short;
typedef	vuint_t	vu_int;
typedef	vulong_t vu_long;
typedef	long swblk_t;
typedef u_long fixpt_t;
typedef long segsz_t;
typedef /*@abstract@*/ fd_set;

int ioctl (int d, int /*@alt long@*/ request, /*@out@*/ void *arg) 
  /*@modifies *arg, errno@*/ ;  /* depends on request! */

/*@constant int BADSIG@*/
/*@constant int SA_ONSTACK@*/
/*@constant int SA_RESTART@*/
/*@constant int SA_DISABLE@*/
/*@constant int SIGBUS@*/
/*@constant int SIGEMT@*/
/*@constant int SIGINFO@*/
/*@constant int SIGIO@*/
/*@constant int SIGIOT@*/
/*@constant int SIGPOLL@*/
/*@constant int SIGPROF@*/
/*@constant int SIGPWR@*/
/*@constant int SIGSYS@*/
/*@constant int SIGTRAP@*/
/*@constant int SIGURG@*/
/*@constant int SIGVTALRM@*/
/*@constant int SIGWINCH@*/
/*@constant int SIGXCPU@*/
/*@constant int SIGXFSZ@*/

extern void psignal (int sig, const char *msg)
  /*@modifies fileSystem@*/;

extern int setenv (const char *name, const char *value, int overwrite)
  /*@globals environ@*/
  /*@modifies *environ, errno@*/;

extern void unsetenv (const char *name)
  /*@globals environ@*/
  /*@modifies *environ@*/;

struct timeval {
  time_t	tv_sec;
  suseconds_t	tv_usec;
} ;

struct timespec {
  time_t tv_sec;
  long	 tv_nsec;
} ;

struct timezone {
  int	tz_minuteswest;
  int	tz_dsttime;
} ;

/*@constant int DST_NONE@*/
/*@constant int DST_USA@*/
/*@constant int DST_AUST@*/
/*@constant int DST_WET@*/
/*@constant int DST_MET@*/
/*@constant int DST_EET@*/
/*@constant int DST_CAN@*/

/*@constant int ITIMER_PROF@*/
/*@constant int ITIMER_REAL@*/
/*@constant int ITIMER_VIRTUAL@*/

struct itimerval {
  struct	timeval it_interval;
  struct	timeval it_value;
};

struct clockinfo {
  int	hz;
  int	tick;
  int	stathz;
  int	profhz;
};

extern int adjtime (const struct timeval *delta, /*@null@*/ /*@out@*/ struct timeval *olddelta)
  /*@modifies internalState, *olddelta, errno@*/;

extern int getitimer (int which, /*@out@*/ struct itimerval *value)
  /*@modifies errno, *value*/;

extern int gettimeofday (/*@null@*/ /*@out@*/ struct timeval *tp, /*@null@*/ /*@out@*/ struct timezone *tzp)
  /*@modifies *tp, *tzp, errno@*/;

extern int setitimer (int which, struct itimerval *val, /*@null@*/ /*@out@*/ struct itimerval *oval)
  /*@modifies *oval, errno, internalState*/;

extern int settimeofday (const struct timeval *t, const struct timezone *z)
  /*@modifies internalState, errno@*/;

extern int utimes (const char *file, /*@null@*/ const struct timeval *times)
  /*@modifies fileSystem, errno*/;

/*
 * sys/mman.h
 */

/*@constant int PROT_READ@*/
/*@constant int PROT_WRITE@*/
/*@constant int PROT_EXEC@*/
/*@constant int MAP_SHARED@*/
/*@constant int MAP_PRIVATE@*/
/*@constant int MAP_COPY@*/
/*@constant int MAP_FIXED@*/
/*@constant int MAP_RENAME@*/
/*@constant int MAP_NORESERVE@*/
/*@constant int MAP_INHERIT@*/
/*@constant int MAP_NOEXTEND@*/
/*@constant int MAP_HASSEMAPHORE@*/
/*@constant int MS_ASYNC@*/
/*@constant int MS_INVALIDATE@*/
/*@constant int MAP_FILE@*/
/*@constant int MAP_ANON@*/
/*@constant int MADV_NORMAL@*/
/*@constant int MADV_RANDOM@*/
/*@constant int MADV_SEQUENTIAL@*/
/*@constant int MADV_WILLNEED@*/
/*@constant int MADV_DONTNEED@*/

extern caddr_t mmap (/*@null@*/ /*@returned@*/ caddr_t addr, size_t len, int prot, int flags, int fd, off_t offset)
  /*@modifies addr@*/;

extern int madvise (caddr_t addr, int len, int behav)
  /*@*/;

extern int mprotect (caddr_t addr, int len, int prot)
  /*@*/;

extern int int munmap (/*@only@*/ caddr_t addr, size_t len)
  /*@modifies fileSystem, *addr, errno @*/;
    
extern int msync (caddr_t addr, int len, int flags)
  /*@*/;

extern int mlock (caddr_t addr, size_t len)
  /*@*/;

extern int munlock (caddr_t addr, size_t len)
  /*@*/;


/*
 * sys/ioctl.h
 */

struct winsize {
  unsigned short	ws_row;
  unsigned short	ws_col;
  unsigned short	ws_xpixel;
  unsigned short	ws_ypixel;
};

/*@constant int TIOCMODG@*/
/*@constant int TIOCMODS@*/
/*@constant int TIOCM_LE@*/
/*@constant int TIOCM_DTR@*/
/*@constant int TIOCM_RTS@*/
/*@constant int TIOCM_ST@*/
/*@constant int TIOCM_SR@*/
/*@constant int TIOCM_CTS@*/
/*@constant int TIOCM_CAR@*/
/*@constant int TIOCM_CD@*/
/*@constant int TIOCM_RNG@*/
/*@constant int TIOCM_RI@*/
/*@constant int TIOCM_DSR@*/
/*@constant int TIOCEXCL@*/
/*@constant int TIOCNXCL@*/
/*@constant int TIOCFLUSH@*/
/*@constant int TIOCGETA@*/
/*@constant int TIOCSETA@*/
/*@constant int TIOCSETAW@*/
/*@constant int TIOCSETAF@*/
/*@constant int TIOCGETD@*/
/*@constant int TIOCSETD@*/
/*@constant int TIOCSBRK@*/
/*@constant int TIOCCBRK@*/
/*@constant int TIOCSDTR@*/
/*@constant int TIOCCDTR@*/
/*@constant int TIOCGPGRP@*/
/*@constant int TIOCSPGRP@*/
/*@constant int TIOCOUTQ@*/
/*@constant int TIOCSTI@*/
/*@constant int TIOCNOTTY@*/
/*@constant int TIOCPKT@*/
/*@constant int TIOCPKT_DATA@*/
/*@constant int TIOCPKT_FLUSHREAD@*/
/*@constant int TIOCPKT_FLUSHWRITE@*/
/*@constant int TIOCPKT_STOP@*/
/*@constant int TIOCPKT_START@*/
/*@constant int TIOCPKT_NOSTOP@*/
/*@constant int TIOCPKT_DOSTOP@*/
/*@constant int TIOCPKT_IOCTL@*/
/*@constant int TIOCSTOP@*/
/*@constant int TIOCSTART@*/
/*@constant int TIOCMSET@*/
/*@constant int TIOCMBIS@*/
/*@constant int TIOCMBIC@*/
/*@constant int TIOCMGET@*/
/*@constant int TIOCREMOTE@*/
/*@constant int TIOCGWINSZ@*/
/*@constant int TIOCSWINSZ@*/
/*@constant int TIOCUCNTL@*/
/*@constant int TIOCSTAT@*/
/*@constant int TIOCCONS@*/
/*@constant int TIOCSCTTY@*/
/*@constant int TIOCEXT@*/
/*@constant int TIOCSIG@*/
/*@constant int TIOCDRAIN@*/
/*@constant int TIOCMSDTRWAIT@*/
/*@constant int TIOCMGDTRWAIT@*/
/*@constant int TIOCTIMESTAMP@*/
/*@constant int TIOCSDRAINWAIT@*/
/*@constant int TIOCGDRAINWAIT@*/
/*@constant int TTYDISC@*/
/*@constant int TABLDISC@*/
/*@constant int SLIPDISC@*/
/*@constant int PPPDISC@*/

/*@constant int MAXHOSTNAMELEN@*/

extern void FD_CLR (/*@sef@*/ int n, /*@sef@*/ fd_set *p) /*@modifies *p@*/ ;
extern void FD_COPY (/*@sef@*/ fd_set *f, /*@out@*/ fd_set *t) /*@modifies *t@*/ ;
extern int /*@alt _Bool@*/ FD_ISSET (/*@sef@*/ int n, /*@sef@*/ fd_set *p) /*@*/ ;
extern void FD_SET (/*@sef@*/ int n, /*@sef@*/ fd_set *p) /*@modifies *p@*/ ;
extern void FD_ZERO (/*@sef@*/ fd_set /*@out@*/ *p) /*@modifies *p@*/;

int initgroups (const char *name, int basegid)
  /*@modifies internalState@*/;

int select (int mfd, fd_set /*@null@*/ *r, fd_set /*@null@*/ *w, 
    fd_set /*@null@*/ *e, /*@null@*/ struct timeval *t)
  /*@modifies *r, *w, *e, *t, errno@*/;

int setegid (gid_t egid)
  /*@modifies errno, internalState@*/;

int seteuid (uid_t euid)
  /*@modifies errno, internalState@*/;
     
int setgroups (int ngroups, const gid_t *gidset)
  /*@modifies errno, internalState@*/;
     
int setregid (gid_t rgid, gid_t egid)
  /*@modifies errno, internalState@*/
  /*:errorcode -1:*/ ;
     
int setreuid (gid_t ruid, gid_t euid)
  /*@modifies errno, internalState@*/
  /*:errorcode -1:*/ ;
     
/*@constant int EBADRPC@*/
/*@constant int ERPCMISMATCH@*/
/*@constant int EPROGUNAVAIL@*/
/*@constant int EPROGMISMATCH@*/
/*@constant int EPROCUNAVAIL@*/
/*@constant int EFTYPE@*/
/*@constant int EAUTH@*/
/*@constant int ENEEDAUTH@*/
/*@constant int ELAST@*/

/*
** tar.h
*/

/*@unchecked@*/ extern char *TMAGIC;
/*@constant int TMAGLEN@*/
/*@unchecked@*/ extern char *TVERSION;
/*@constant int TVERSLEN@*/

/*@constant int REGTYPE@*/
/*@constant int AREGTYPE@*/
/*@constant int LNKTYPE@*/
/*@constant int SYMTYPE@*/
/*@constant int CHRTYPE@*/
/*@constant int BLKTYPE@*/
/*@constant int DIRTYPE@*/
/*@constant int FIFOTYPE@*/
/*@constant int CONTTYPE@*/

/*@constant int TSUID@*/
/*@constant int TSGID@*/
/*@constant int TSVTX@*/

/*@constant int TUREAD@*/
/*@constant int TUWRITE@*/
/*@constant int TUEXEC@*/
/*@constant int TGREAD@*/
/*@constant int TGWRITE@*/
/*@constant int TGEXEC@*/
/*@constant int TOREAD@*/
/*@constant int TOWRITE@*/
/*@constant int TOEXEC@*/

struct ipc_perm {
  uid_t	uid;	/* user id */
  gid_t	gid;	/* group id */
  uid_t	cuid;	/* creator user id */
  gid_t	cgid;	/* creator group id */
  mode_t mode;	/* r/w permission */
  ulong	seq;	/* slot usage sequence number */
  key_t	key;	/* user specified msg/sem/shm key */
} ;

/*@constant int	IPC_R@*/
/*@constant int	IPC_W@*/
/*@constant int	IPC_M@*/
/*@constant int	IPC_CREAT@*/
/*@constant int	IPC_EXCL@*/
/*@constant int	IPC_NOWAIT@*/
/*@constant key_t IPC_PRIVATE@*/
/*@constant int IPC_RMID@*/
/*@constant int IPC_SET@*/
/*@constant int IPC_STAT@*/

/*
** sys/msg.h
*/

struct msqid_ds {
  struct	ipc_perm msg_perm;	/* msg queue permission bits */
  struct	msg *msg_first;	/* first message in the queue */
  struct	msg *msg_last;	/* last message in the queue */
  u_long	msg_cbytes;	/* number of bytes in use on the queue */
  u_long	msg_qnum;	/* number of msgs in the queue */
  u_long	msg_qbytes;	/* max # of bytes on the queue */
  pid_t	msg_lspid;	/* pid of last msgsnd() */
  pid_t	msg_lrpid;	/* pid of last msgrcv() */
  time_t	msg_stime;	/* time of last msgsnd() */
  time_t	msg_rtime;	/* time of last msgrcv() */
  time_t	msg_ctime;	/* time of last msgctl() */
};

struct mymesg {
  long	mtype;		/* message type (+ve integer) */
  char	mtext[];	/* message body */
};

/*@constant int MSG_NOERROR@*/
/*@constant int MSGMAX@*/
/*@constant int MSGMNB@*/
/*@constant int MSGMNI@*/
/*@constant int MSGTQL@*/

extern int msgctl (int id , int cmd, /*@out@*/ struct msqid_ds *buf)
  /*@modifies errno, *buf@*/;

extern int msgget (key_t key, int flag)
  /*@modifies errno@*/;

extern int msgrcv (int id, /*@out@*/ void *ptr, size_t nbytes, long type, int flags)
  /*@modifies errno, *ptr@*/;

extern int msgsnd (int id, const void *ptr, size_t nbytes, int flag)
  /*@modifies errno@*/;

/*
** sys/sem.h
*/

struct semid_ds {
  struct ipc_perm sem_perm;
  struct sem     *sem_base;
  ushort          sem_nsems;
  time_t          sem_otime;
  time_t          sem_ctime;
};

struct sem {
  ushort semval;
  pid_t  sempid;
  ushort semncnt;
  ushort semzcnt;
};

union semun {
  int val;
  struct semid_ds *buf;
  ushort *array;
};

struct sembuf {
  ushort sem_num;
  short sem_op;
  short sem_flg;
};

/*@constant int SEM_A@*/
/*@constant int SEMAEM@*/
/*@constant int SEMMNI@*/
/*@constant int SEMMNS@*/
/*@constant int SEMMNU@*/
/*@constant int SEMMSL@*/
/*@constant int SEMOPN@*/
/*@constant int SEM_R@*/
/*@constant int SEMUME@*/
/*@constant int SEM_UNDO@*/
/*@constant int SEMVMX@*/
/*@constant int GETVAL@*/
/*@constant int SETVAL@*/
/*@constant int GETPID@*/
/*@constant int GETNCNT@*/
/*@constant int GETZCNT@*/
/*@constant int GETALL@*/
/*@constant int SETALL@*/

/*@constant int ERMID@*/

extern int semctl (int id, int semnum, int cmd, union semun arg)
  /*@modifies errno@*/;

extern int semget (key_t key, int nsems, int flag)
  /*@modifies errno@*/;

extern int semop (int id, struct sembuf *semoparray, size_t nops)
  /*@modifies errno@*/;

/*
** sys/shm.h
*/

struct shmid_ds {
  struct ipc_perm shm_perm;
  int shm_segsz;
  ushort shm_lkcnt;
  pid_t shm_lpid;
  pid_t shm_cpid;
  ulong shm_nattch;
  ulong shm_cnattch;
  time_t shm_atime;
  time_t shm_dtime;
  time_t shm_ctime;
};

/*@constant int SHMLBA@*/
/*@constant int SHM_LOCK@*/
/*@constant int SHMMAX@*/
/*@constant int SHMMIN@*/
/*@constant int SHMMNI@*/
/*@constant int SHM_R@*/
/*@constant int SHM_RDONLY@*/
/*@constant int SHM_RND@*/
/*@constant int SHMSEG@*/
/*@constant int SHM_W@*/
/*@constant int SHM_UNLOCK@*/

void * shmat (int id, /*@null@*/ void *addr, int flag)
  /*@modifies errno@*/ ;
     
extern int shmctl (int id, int cmd, /*@out@*/ struct shmid_ds *buf)
  /*@modifies errno, *buf@*/ ;

extern int shmdt (void *addr)
  /*@modifies errno@*/ ;

extern int shmget (key_t key, int size, int flag)
  /*@modifies errno@*/ ;


/*
** syslog.h
*/

/*@constant int LOG_EMERG@*/
/*@constant int LOG_ALERT@*/
/*@constant int LOG_CRIT@*/
/*@constant int LOG_ERR@*/
/*@constant int LOG_WARNING@*/
/*@constant int LOG_NOTICE@*/
/*@constant int LOG_INFO@*/
/*@constant int LOG_DEBUG@*/

/*@constant int LOG_KERN@*/
/*@constant int LOG_USER@*/
/*@constant int LOG_MAIL@*/
/*@constant int LOG_DAEMON@*/
/*@constant int LOG_AUTH@*/
/*@constant int LOG_SYSLOG@*/
/*@constant int LOG_LPR@*/
/*@constant int LOG_NEWS@*/
/*@constant int LOG_UUCP@*/
/*@constant int LOG_CRON@*/
/*@constant int LOG_AUTHPRIV@*/
/*@constant int LOG_FTP@*/
/*@constant int LOG_LOCAL0@*/
/*@constant int LOG_LOCAL1@*/
/*@constant int LOG_LOCAL2@*/
/*@constant int LOG_LOCAL3@*/
/*@constant int LOG_LOCAL4@*/
/*@constant int LOG_LOCAL5@*/
/*@constant int LOG_LOCAL6@*/
/*@constant int LOG_LOCAL7@*/

/*@constant int LOG_PID@*/
/*@constant int LOG_CONS@*/
/*@constant int LOG_ODELAY@*/
/*@constant int LOG_NDELAY@*/
/*@constant int LOG_NOWAIT@*/
/*@constant int LOG_PERROR@*/

int LOG_MASK (int pri) /*@*/;
     
int LOG_UPTO (int pri) /*@*/;
     
void closelog (void) /*@modifies fileSystem@*/;
     
void openlog (const char *ident, int logopt, int facility)
  /*@modifies fileSystem@*/;
     
int setlogmask (int maskpri)
  /*@modifies internalState@*/;
     
void /*@printflike@*/ syslog (int priority, const char *message, ...)
  /*@modifies fileSystem@*/;
     
void vsyslog (int priority, const char *message, va_list args)
  /*@modifies fileSystem@*/;

/*
 * pwd.h
 */

extern extern void endpwent (void)
  /*@modifies internalState@*/;

extern /*@null@*/ struct passwd * getpwent (void)
  /*@modifies internalState@*/;

extern int setpassent (int stayopen)
  /*@modifies internalState@*/;

extern int setpwent (void)
  /*@modifies internalState@*/;

/*
** grp.h
*/

void endgrent (void) /*@modifies internalState@*/;

/*@null@*/ /*@observer@*/ struct group *getgrent (void)
  /*@modifies internalState@*/;

int setgrent (void) /*@modifies internalState@*/;

void setgrfile (const char *name) /*@modifies internalState@*/;

int setgroupent (int stayopen) /*@modifies internalState@*/;

/*
** sys/stat.h
**
** from http://www.opengroup.org/onlinepubs/007908799/xsh/sysstat.h.html
*/

/*
** struct stat replaces POSIX version - more required fields in Unix
*/

/*@constant mode_t S_IWUSR@*/
/*@constant mode_t S_IXUSR@*/
/*@constant mode_t S_IRWXG@*/
/*@constant mode_t S_IRGRP@*/
/*@constant mode_t S_IWGRP@*/
/*@constant mode_t S_IXGRP@*/
/*@constant mode_t S_IRWXO@*/
/*@constant mode_t S_IROTH@*/
/*@constant mode_t S_IWOTH@*/
/*@constant mode_t S_IXOTH@*/
/*@constant mode_t S_ISUID@*/
/*@constant mode_t S_ISGID@*/
/*@constant mode_t S_ISVTX@*/

# if 0
/*These are the old definitions - they don't appear to be in the Single UNIX Specification */

/*@constant int S_ISTXT@*/
/*@constant int S_IREAD@*/
/*@constant int S_IWRITE@*/
/*@constant int S_IEXEC@*/
/*@constant int S_IFMT@*/
/*@constant int S_IFIFO@*/
/*@constant int S_IFCHR@*/
/*@constant int S_IFDIR@*/
/*@constant int S_IFBLK@*/
/*@constant int S_IFREG@*/
/*@constant int S_IFLNK@*/
/*@constant int S_IFSOCK@*/
/*@constant int S_ISVTX@*/
/*@constant int SF_SETTABLE@*/
/*@constant int SF_ARCHIVED@*/
/*@constant int ACCESSPERMS@*/
/*@constant int ALLPERMS@*/
/*@constant int DEFFILEMODE@*/
/*@constant int S_BLKSIZE@*/
/*@constant int SF_IMMUTABLE@*/
/*@constant int SF_APPEND@*/
/*@constant int UF_NODUMP@*/
/*@constant int UF_IMMUTABLE@*/
/*@constant int UF_APPEND@*/
# endif

int /*@alt _Bool@*/ S_ISBLK (/*@sef@*/ mode_t m) /*@*/;
int /*@alt _Bool@*/ S_ISCHR (/*@sef@*/ mode_t m) /*@*/;
int /*@alt _Bool@*/ S_ISDIR (/*@sef@*/ mode_t m) /*@*/;
int /*@alt _Bool@*/ S_ISFIFO (/*@sef@*/ mode_t m) /*@*/;
int /*@alt _Bool@*/ S_ISREG (/*@sef@*/ mode_t m) /*@*/;
int /*@alt _Bool@*/ S_ISLNK (/*@sef@*/ mode_t m) /*@*/;

int /*@alt _Bool@*/ S_TYPEISMQ (/*@sef@*/ struct stat *buf) /*@*/ ;
int /*@alt _Bool@*/ S_TYPEISSEM (/*@sef@*/ struct stat *buf) /*@*/ ;
int /*@alt _Bool@*/ S_TYPEISSHM  (/*@sef@*/ struct stat *buf) /*@*/ ;

int lstat(const char *, /*@out@*/ struct stat *)
     /*:errorcode -1:*/
     /*@modifies errno@*/ ;
     
int mknod (const char *, mode_t, dev_t)
  /*@warn portability "The only portable use of mknod is to create FIFO-special file. If mode is not S_IFIFO or dev is not 0, the behaviour of mknod() is unspecified."@*/
  /*:errorcode -1:*/
  /*@modifies errno@*/ ;

int chflags (const char *path, u_long flags)
  /*@warn unixstandard "Not in Single UNIX Specification Version 2"@*/
  /*@modifies fileSystem, errno@*/;

int fchflags (int fd, u_long flags)
  /*@warn unixstandard "Not in Single UNIX Specification Version 2"@*/
  /*@modifies fileSystem, errno@*/;

int fchmod(int fildes, mode_t mode) 
   /*@modifies fileSystem, errno@*/ ; 
  
/*
** sys/statvfs.h
**
** from http://www.opengroup.org/onlinepubs/007908799/xsh/sysstatvfs.h.html
*/

struct statvfs {
   unsigned long f_bsize;
   unsigned long f_frsize;
   fsblkcnt_t    f_blocks;
   fsblkcnt_t    f_bfree;
   fsblkcnt_t    f_bavail;
   fsfilcnt_t    f_files;
   fsfilcnt_t    f_ffree;
   fsfilcnt_t    f_favail;                       
   unsigned long f_fsid;
   unsigned long f_flag;
   unsigned long f_namemax; 
} ;

/*@constant unsigned long ST_RDONLY; @*/
/*@constant unsigned long ST_NOSUID; @*/

int fstatvfs (int fildes, /*@out@*/ struct statvfs *buf) 
  /*@modifies buf@*/ ;

int statvfs (const char *path, /*@out@*/ struct statvfs *buf)
  /*@modifies buf@*/ ; 


/*
 * stropts.h
 */

/*@constant int FMNAMESZ@*/
/*@constant int MSG_BAND@*/
/*@constant int MSG_HIPRI@*/
/*@constant int RS_HIPRI@*/
/*@constant int S_INPUT@*/
/*@constant int S_RDNORM@*/
/*@constant int S_RDBAND@*/
/*@constant int S_BANDURG@*/
/*@constant int S_HIPRI@*/
/*@constant int S_OUTPUT@*/
/*@constant int S_WRNORM@*/
/*@constant int S_WRBAND@*/
/*@constant int S_MSG@*/
/*@constant int S_ERROR@*/
/*@constant int S_HANGUP@*/

struct strbuf {
  int maxlen;
  int len;
  char *buf;
}

struct str_mlist {
  char l_name[];
}

struct str_list {
  int sl_nmods;
  struct str_mlist *sl_modlist;
}

extern int getmsg (int fd, /*@out@*/ struct strbuf *c, /*@out@*/ struct strbuf *d, int *f)
  /*@modifies *c, *d, errno@*/;

extern int getpmsg (int fd, /*@out@*/ struct strbuf *c, /*@out@*/ struct strbuf *d, int *b, int *f)
  /*@modifies *b, *c, *d, errno@*/;

extern int putmsg (int fd, const struct strbuf *c, const struct strbuf *d, int *f)
  /*@modifies internalState, errno@*/;

extern int putpmsg (int fd, const struct strbuf *c, const struct strbuf *d, int b, int *f)
   /*@modifies internalState, errno@*/;

/*
** sys/resource.h
**
** from http://www.opengroup.org/onlinepubs/007904975/basedefs/sys/resource.h.html
*/

/*@constant int PRIO_PROCESS@*/
/*@constant int PRIO_PGRP@*/
/*@constant int PRIO_USER@*/

typedef /*@unsignedintegraltype@*/ rlim_t;

/*@constant rlim_t RLIM_INFINITY@*/
/*@constant rlim_t RLIM_SAVED_MAX@*/
/*@constant rlim_t RLIM_SAVED_CUR@*/

/*@constant int RUSAGE_SELF@*/
/*@constant int RUSAGE_CHILDREN@*/

struct rlimit {
  rlim_t rlim_cur;
  rlim_t rlim_max;
};

struct rusage {
  struct timeval ru_utime;        /* user time used */
  struct timeval ru_stime;        /* system time used */
  /* other members optional */
};

/*@constant int RLIMIT_CORE@*/
/*@constant int RLIMIT_CPU@*/
/*@constant int RLIMIT_DATA@*/
/*@constant int RLIMIT_FSIZE@*/
/*@constant int RLIMIT_NOFILE@*/
/*@constant int RLIMIT_STACK@*/
/*@constant int RLIMIT_AS@*/

int getpriority (int which, id_t who)
   /*@modifies errno@*/;

int getrlimit (int res, /*@out@*/ struct rlimit *rlp)
   /*@modifies *rlp, errno@*/;

int getrusage (int who, /*@out@*/ struct rusage *rusage)
   /*@modifies *rusage, errno@*/;

int setpriority (int which, id_t who, int prio)
   /*@modifies errno, internalState@*/;

int setrlimit (int resource, const struct rlimit *rlp)
   /*@modifies errno, internalState@*/;

/*
** in <netdb.h>
*/

struct servent
{
  /*@observer@*/ char *s_name;			/* Official service name.  */
  /*@observer@*/ char **s_aliases;		/* Alias list.  */
  int s_port;			                /* Port number.  */
  /*@observer@*/ char *s_proto;		        /* Protocol to use.  */
} ;

/*@observer@*/ /*@dependent@*/ /*@null@*/ struct servent *
getservbyname (const char *name, /*@null@*/ const char *proto) 
  /*@warn multithreaded "Unsafe in multithreaded applications, use getsrvbyname_r instead"@*/ ;

struct servent *
getservbyname_r (const char *name, /*@null@*/ const char *proto,
    /*@out@*/ /*@returned@*/ struct servent *result, /*@out@*/ char *buffer, int buflen) 
  /*@requires maxSet (buffer) >= buflen@*/ ;

/*@observer@*/ /*@dependent@*/ struct servent *
getservbyport (int port, /*@null@*/ const char *proto)
  /*@warn multithreaded "Unsafe in multithreaded applications, use getservbyport_r instead"@*/ ;

struct servent *
getservbyport_r (int port, /*@null@*/ const char *proto,
    /*@out@*/ /*@returned@*/ struct servent *result, /*@out@*/ char *buffer, int buflen)
  /*@requires maxSet (buffer) >= buflen@*/ ;

/*@null@*/ struct servent *getservent (void);

/*@null@*/ struct servent *getservent_r (struct servent *result, char *buffer, int buflen);

int setservent (int stayopen);
int endservent (void);

extern int h_errno;

/*@null@*/ /*@observer@*/
struct hostent * gethostbyname (/*@nullterminated@*/ /*@notnull@*/ const char *name)
  /*@warn multithreaded "Unsafe in multithreaded applications, use gethostbyname_r instead"@*/ 
  /*@modifies h_errno@*/ ;

struct hostent *
gethostbyname_r (/*@nullterminated@*/ const char *name,
    /*@notnull@*/ /*@returned@*/ struct hostent *hent,
    /*@out@*/ /*@exposed@*/ char *buffer, int bufsize, /*@out@*/ int *h_errnop)
  /*@requires maxSet(buffer) >= bufsize@*/ ;

/*@null@*/ /*@observer@*/
struct hostent *gethostbyaddr (/*@notnull@*/ const void *addr, size_t addrlen, int type) 
     /*@requires maxRead(addr) == addrlen@*/ /*:i534 ??? is this right? */
     /*@warn multithreaded "Unsafe in multithreaded applications, use gethostbyaddr_r instead"@*/ 
     /*@modifies h_errno@*/ ;

struct hostent *gethostbyaddr_r (/*@notnull@*/ const void *addr, size_t addrlen, int type, 
				 /*@returned@*/ /*@out@*/ struct hostent *hent, 
				 /*@exposed@*/ /*@out@*/ char *buffer, int bufsize, /*@out@*/ int *h_errnop)
  /*@requires maxRead(addr) == addrlen /\ maxSet(buffer) >= bufsize@*/
  /*:i534 ??? is this right? */ ;

/*@observer@*/ /*@null@*/
struct hostent *gethostent (void)
  /*@warn multithreaded "Unsafe in multithreaded applications, use gethostent_r instead"@*/ ;

struct hostent *gethostent_r (/*@out@*/ /*@returned@*/ struct hostent *hent, /*@exposed@*/ /*@out@*/ char *buffer, int bufsize)
  /*@requires maxSet(buffer) >= bufsize@*/ ;

/*:i534 need to annotate these: */

struct hostent *fgethostent(FILE *f);
struct hostent *fgethostent_r(FILE*f, struct hostent *hent, char buffer, int bufsize);
void sethostent(int stayopen);
void endhostent(void);
void herror(const char *string);
char *hstrerror(int err);

struct hostent {
  /*@observer@*/ /*@nullterminated@*/ char *h_name;   /* official name of host */
  /*@observer@*/ /*@nullterminated@*/ char * /*:observer@*/ /*:nullterminated@*/ *h_aliases;   /* alias list */
  int  h_addrtype;   /* host address type*/
  int  h_length;   /* length ofaddress*/
  /*@observer@*/ /*@nullterminated@*/ char * /*:observer@*/ /*:nullterminated@*/ *h_addr_list; /* list of addressesfrom name server */
  /*@observer@*/ /*@nullterminated@*/ char *h_addr; /* first address in list (backward compatibility) */
} ;

/*
** netinet/in.h
**
** from http://www.opengroup.org/onlinepubs/007908799/xns/netinetin.h.html
*/

typedef /*@unsignedintegraltype@*/ in_port_t; /* An unsigned integral type of exactly 16 bits. */
typedef /*@unsignedintegraltype@*/ in_addr_t; /* An unsigned integral type of exactly 32 bits. */

/* sa_family_t moved earlier */

struct in_addr {
  in_addr_t      s_addr;
} ;

struct sockaddr_in {
  sa_family_t    sin_family;
  in_port_t      sin_port;
  struct in_addr sin_addr;
  unsigned char  sin_zero[8];
} ;


/* The <netinet/in.h> header defines the following macros for use as values of
   the level argument of getsockopt() and setsockopt(): */

/*@constant int IPPROTO_IP@*/
/*@constant int IPPROTO_ICMP@*/
/*@constant int IPPROTO_TCP@*/
/*@constant int IPPROTO_UDP@*/

/* The <netinet/in.h> header defines the following macros for use as destination
   addresses for connect(), sendmsg() and sendto(): */

/*@constant in_addr_t INADDR_ANY@*/
/*@constant in_addr_t INADDR_BROADCAST@*/

/*
** arpa/inet.h
*/

in_addr_t htonl (in_addr_t hostlong) /*@*/ ;
in_port_t htons (in_port_t hostshort) /*@*/ ;
in_addr_t ntohl (in_addr_t netlong) /*@*/ ;
in_port_t ntohs (in_port_t netshort) /*@*/ ;

/* not sure what the exact size of this is; also can IPv6 use this function?
   I'm going to assume that the address had the format:
       "###.###.###.###" which is 16 bytes */
/*@kept@*/ char *inet_ntoa(struct in_addr in)
  /*@ensures maxSet(result) <= 15 /\ maxRead(result) <= 15 @*/
  ;


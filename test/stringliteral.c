
char buf00[8] = "\000\000\000\000\000\000\000\000";
char buf01[8] = "\000\000\000\000\000\000\000\001";
char buf02[4] = "abcd";
char buf03[4] = "ab\n";
char buf04[4] = "\034\342\24";
char buf05[4] = "abcdef";

char buf06[7] = "abc""def";
char buf07[7] = "abc" /*comment*/ "def";
char buf08[7] = "abc"			    "def";
char buf09[7] = "abc"	
        
	"def";
char buf10[8] = "abc def";
char buf11[8] = "abc " "def";
char buf12[8] = "abc" " " "def";
char buf13[8] = "abc" " def";
char buf14[8] = "abc" "def ";

char buf15[] =
         "\t0 abcdefghijklmnopqrstuvwxyz\n"
         "\t1 abcdefghijklmnopqrstuvwxyz\n"
         "\t2 abcdefghijklmnopqrstuvwxyz\n"
         "\t3 abcdefghijklmnopqrstuvwxyz\n"
         "\t4 abcdefghijklmnopqrstuvwxyz\n"
         "\n";


/* Note: check if bug triggers with a classic redefinition. */
#define	SYS_CONST_TWO		42

/* Note: check if bug triggers with a more convoluted redefinition. */
#define SYS_CONST_ONE		SYS_CONST_ONE

/* Note: check if bug triggers with a constant not specified in system library. */
enum
  {
    SYS_CONST_NEW
  };
#define	SYS_CONST_NEW		SYS_CONST_NEW

/* Note: this triggers the bug. */
enum
  {
    SYS_CONST_ZERO
  };
#define	SYS_CONST_ZERO		SYS_CONST_ZERO


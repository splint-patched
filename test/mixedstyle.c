/*
 * Previous changes to the way old style parameter list was handled impacted the
 * way parameters of (pointerless) function type were handled; it didn't impact
 * pointer function type as parameter, though under the hood pointerless
 * function types are converted to pointer function type.  To prevent changes
 * that seem to work for mixed (new & old) style parameter list but mess the
 * function type as parameter, added first two routines, just to make sure any
 * further changes don't affect them.
 */

char f0 (char (*morph) (int), int val)
{
    return (val > 0) ? morph (0+val) : morph (0-val);
}

char f1 (char (morph) (int), int val)
{
    return (val > 0) ? morph (0+val) : morph (0-val);
}

/* The actual routines with mixed style parameters list. */

int f2 (x, char y, char z)
{
  return y > z ? (0+x) : (0-x);
}

/*
 * Parse error are NOT reported if placed less then 5 lines from the last
 * reported error; thus we need to add some white-space, to make the next parse
 * error visible.
 */

int f3 (char y, char z, x)
{
  return y > z ? (0+x) : (0-x);
}


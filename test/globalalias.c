/*
 * Modified version of source buggy example, itself a contrieved example:
 *   https://sourceforge.net/p/splint/bugs/15
 * 
 * If alias info is incorrectly clean-up for globals (in this case variable A)
 * in one function (create_a in this case), storage references valid only for
 * that function (they are freed at the end of every function) might get
 * accessed in another function (free_a in this case), causing reads of invalid
 * memory -- and incorrect/unexpected behaviour.
 */

struct a {
  struct a *next;
} ;

struct a *A;

struct a *
create_a(void)
{
  struct a *p;
  A = p->next;
  p->next = NULL;
  return p;
}

void
free_a(struct a *p)
{
  p->next = A; /* Shouldn't show alias info from create_a(). */
}


# include <stdio.h>

int main (void) {
  int i;

  printf ("a%n\n", &i); 

  printf ("%d\n", i);

  /* strings are properly spliced before format check */
  printf("%" "i", i);

  /* trigger (args order) warning - check correct location */
  printf("inputs:"
      "\t++i = %i,\t"
      "i = %i\n", i++, i);

  return 0;
}


#include <stdio.h>

#define m(o, ...)   fprintf(o, __VA_ARGS__)
#define merr(...)   fprintf(stderr, __VA_ARGS__)

/* gnu-extension: name for the variadic arguments list */
#define n(o, args...) fprintf(o, args)
#define nerr(args...) fprintf(stderr, args)

int
main(void)
{
    m(stderr, "1 = %s\n", "one");
    merr("2 = %s\n", "two");

    n(stderr, "3 = %s\n", "three");
    nerr("4 = %s\n", "four");

    return 0;
}


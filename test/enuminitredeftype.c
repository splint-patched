/* redefinition of datatype by enum field */

typedef struct {
  int i;
} t;

enum e {
  s = 0,
  t
};

/* redefinition of datatype by enum field inside function */

typedef struct {
    char f_c;
    unsigned int f_ui;
} f_t;

bool
f(int f_i)
{
    enum f_e {
        f_s = 0,
        f_t,
        f_x
    };

    return f_i/2 == f_x;
}


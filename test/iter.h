# define intFor(i, j, k, ___x) \
  { int ___x = (i);  while ((___x) <= (j)) { (___x) += (k); 

# define end_intFor }}

# define arrayElements(i,j,a,___x) \
 { int ___c; int *(___b) = a; for (___c = (i); ___c <= (j); ___c++) { int ___x = *(___b); 
/* 8. Macro parameter j used more than once (in post loop test) */

# define end_arrayElements \
  (___b)++; }}


#include <stdio.h> /* printf() */
#include <string.h> /* strlen() */

#ifdef GNU
void f (void)
{
  printf("(GNU) function name: %s\n", __FUNCTION__);
  printf("(GNU) pretty function name: %s\n", __PRETTY_FUNCTION__);
}
#else
void f (void)
{
  size_t len;

  printf("function name: %s\n", __func__);

  /* evans 2001-12-30: constant, not literal; reported by Jim Zelenka. */
  printf("(spliced) function name: " __func__ "\n");

  len = strlen(__func__);
  printf("this function (%s) has a name that is %u characters long\n",
	 __func__, (unsigned) len);
}
#endif

int
main(void)
{
  f ();
  return 0;
}

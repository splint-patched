#ifndef SPLINT_BOOL_H
#define SPLINT_BOOL_H

/*@-cppnames@*/
/*@-exporttype@*/
typedef /*@abstract@*/ int bool;
/*@=exporttype@*/
/*@=cppnames@*/

#ifndef FALSE
/*@constant unused bool FALSE@*/
#define FALSE false
#endif

#ifndef TRUE
/*@constant unused bool TRUE@*/
#define TRUE true
#endif

# endif /* SPLINT_BOOL_H */

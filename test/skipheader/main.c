/*
 * Test to see if all standard/posix headers are recognized and skipped, given
 * the appropriate flags.
 */

/* skip-iso-headers */
#include <assert.h> 
#include <complex.h>
#include <ctype.h>
#include <errno.h>
#include <fenv.h>
#include <float.h>
#include <inttypes.h>
#include <iso646.h>
#include <limits.h>
#include <locale.h>
#include <math.h> 
#include <setjmp.h>
#include <signal.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h> /* some systems use this...they shouldn't */
#include <tgmath.h>
#include <time.h>
#include <wchar.h>
#include <wctype.h>

/* skip-posix-headers */
#include <dirent.h>
#include <fcntl.h>
#include <grp.h>
#include <pwd.h>
#include <regex.h>
#include <sys/stat.h>
#include <sys/times.h>
#include <sys/types.h>
#include <netdb.h> /* unix */
#include <netinet/in.h> /* unix */
#include <sys/resource.h> /* unix */
#include <sys/socket.h> /* not posix */
#include <sys/syslog.h> /* not posix */
#include <sys/utsname.h>
#include <sys/wait.h>
#include <termios.h>
#include <unistd.h>
#include <utime.h>

int
main(void)
{
    return 0;
}


/*
 * Origin: https://bugs.debian.org/626552
 *
 * Specification keywords should be treated as regular Id when specification
 * keyword not expected.
 */

bool dependent;

int f(char *out, int * fallthrough)
    /*@globals undef dependent@*/
    /*@requires maxSet(out) >= 1;@*/
    /*@ensures maxSet(fallthrough) >= 0;@*/
    ;

int g(void) /*@warn out "blah blah blah ..." @*/ ;


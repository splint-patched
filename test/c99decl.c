#include <stdbool.h>

int f0 (int m, int n)
{
  int i, j;
  i = m / 2;
  j = n * 3;

  int k = j / i, l = k * 5 + 7;
  int x;

  x = (l < 0 ? 0 - l : l);

  return x - (i * j);
}

int f1 (int m, int n)
{
  if (m < 0)
    m = 0 - m;
  if (n < 0)
    n = 0 - n;

  int sum;
  int x = (m > n) ? ((m + 1) / (n + 1)) : ((n+1) / (m+1));
  sum = 0;
  while (x > 0)
  {
    sum += (x % 2 == 1) ? m : n;
    int k;
    k = sum % 2;
    x -= k;
  }

  return sum;
}

int f2 (int m, int n)
{
  if (m < 0)
    m = 0 - m;
  if (n < 0)
    n = 0 - n;

  int tmp;
  if (n > m)
  {
    tmp = m;
    m = n;
    n = tmp;
  }

  if (m == n)
    return 0;

  do {
    int i;
    i = m % n;
    if (i == 0)
      return 0;
    m = m - i;
    int newtmp;
    if (n > m)
    {
      newtmp = m;
      m = n;
      n = newtmp;
    }
  } while (m > n);

  return tmp % n;
}

int f3 (int m, int n)
{
  int j;
  for (int i = 0; i < m * n; i++)
  {
    j = i * i + m + n;
    bool k;
    k = j > m * n;
    if (k)
      return i * i;;
  }
  return j;
}

int f4 (int m, int n)
{
  if (m < 0)
    m = 0 - m;
  if (n < 0)
    n = 0 - n;
  if (m < n) {
    int tmp;
    tmp = m;
    m = n;
    n = tmp;
  }

  int i = m / 5, j = n / 7;

  switch ((i + j) % 3)
  {
    m += n / 13;
    n += m / 11;
    int l;

  case 0:
    l = m - n;
    int x0 = l * l + i * j;
    return x0 - m;

  case 1:
    l = m + n;
    int x1;
    x1 = l / i + l / j;
    return x1 + i * i;

  case 2:
    l = m * n - m % (n + 1);
    int x2 = l - i*i - i*j - j*j, y = x2 - 100;
    return x2 % y;
  }
}

/* testing variable declaration in for init expression */

int f5 (int x, int y)
{
    int collect = 0;
    for (int i = x; i < y; i++)
        collect += i;
    return collect;
}

int f6 (int x, int y)
{
    int collect = 0;
    for (int i = x, j = 0; i < y; i++, j++)
    {
        if (j%2 == 0)
            collect += i;
        else
            collect -= i;
    }
    return collect;
}

void f7 (void)
{
    char t[11];
    t[0] = '\a';
    char s[11];
    for (int i = 0; i <= 10; i++)
        s[i] = '\0';

    for (int i = 0; i <= 11; i++)
        t[i] = s[i];
}

